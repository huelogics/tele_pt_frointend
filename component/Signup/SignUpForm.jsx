import React, { useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";

const SignupForm = () => {
  // State hooks to manage form inputs and error state
  const [name, setName] = useState("");
  const [firstname, setFirstName] = useState("");
  const [lastname, setLastName] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const [organization, setOrganization] = useState("");
  const [otp, setOtp] = useState("");
  const [showComponent, setShowComponent] = useState(false); // State to toggle between signup and OTP verification
  const router = useRouter();

  // Function to handle signup
  const handleSignup = async () => {
    try {
      // Check if all fields are filled and passwords match
      if (
        firstname &&
        lastname &&
        email &&
        organization &&
        password === confirmPassword
      ) {
        const res = await axios.post(
          "https://tele-backend.huelogics.com/sign_up",
          {
            first_name: firstname,
            last_name: lastname,
            email,
            organization,
            password: confirmPassword,
          }
        );
        if (res.status === 200) {
          setShowComponent(true); // Show OTP verification component on successful signup
        }
      }
    } catch (error) {
      console.error(error);
      setError("Signup failed. Please try again.");
    }
  };

  // Function to verify OTP
  const verifyOtp = async () => {
    try {
      if (email) {
        const res = await axios.post(
          "https://tele-backend.huelogics.com/verify_otp",
          {
            email: email,
            otp: otp,
          }
        );
        if (res) {
          setShowComponent(false); // Hide OTP verification component
          router.push(`/patient`); // Navigate to patient page on successful OTP verification
        }
      }
    } catch (error) {
      console.log(error);
      setError("OTP verification failed. Please try again.");
    }
  };

  return (
    <div className="lg:container lg:flex-row flex-col font-sans flex justify-center lg:items-center lg:h-screen overflow-hidden">
      {/* Left side with gradient background */}
      <div className="lg:mx-auto lg:max-w-lg flex-auto lg:h-screen bg-gradient-to-r from-cyan-400 to-indigo-400 text-white relative flex flex-col justify-center items-center">
        <img
          src="/Logo.png"
          alt="Logo"
          className="absolute top-4 left-4 shadow-sm"
        />
        <div className="w-full p-8 flex flex-col justify-center items-center">
          <p className="lg:text-2xl lg:mb-72">
            Already have an Account <br />{" "}
            <span className="ml-8">Please Sign in</span>
          </p>
          <button
            className="mt-9 px-10 py-4 w-40 font-semibold text-white border border-white hover:bg-white hover:text-black rounded-3xl"
            onClick={() => router.back()}
          >
            Sign In
          </button>
        </div>
      </div>

      {showComponent ? (
        // OTP Verification component
        <div className="flex-auto flex justify-center items-center bg-white text-black min-h-screen font-sans">
          <div className="flex flex-col gap-5 items-center w-full">
            <h4 className="text-gray-400 font-medium text-sm text-center">
              We have sent you an email with the OTP on{" "}
              <span className="text-black">{email}</span>
            </h4>
            <input
              maxLength={6}
              type="text"
              id="otp"
              onWheel={() => document.activeElement.blur()}
              onChange={(e) => {
                setOtp(e.target.value);
              }}
              className="bg-gray-50 border border-gray-300 rounded-lg p-2 w-72 outline-none"
            />
            <button
              onClick={verifyOtp}
              type="submit"
              className="bg-blue-400 text-white font-medium text-lg w-72 font-sans py-2 rounded-lg"
            >
              Submit
            </button>
          </div>
        </div>
      ) : (
        // Signup Form component
        <div className="flex-auto flex justify-center items-center bg-white text-black min-h-screen font-sans">
          <div className="p-8 w-full max-w-md flex flex-col justify-center items-center">
            <h2 className="text-3xl mb-11 text-center font-semibold">
              Create Account
            </h2>
            <div className="space-y-6">
              <div className="flex flex-col md:flex-row space-y-6 md:space-y-0 md:space-x-6">
                <div className="flex flex-col w-full">
                  <label className="mb-1">First Name</label>
                  <input
                    type="text"
                    id="firstName"
                    className="border shadow-md w-72 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                </div>
                <div className="flex flex-col w-full">
                  <label className="block mb-1">Last Name</label>
                  <input
                    type="text"
                    id="lastName"
                    className="border shadow-md w-72 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                    onChange={(e) => setLastName(e.target.value)}
                  />
                </div>
              </div>
              {/* Email and Organization */}
              <div className="flex flex-col md:flex-row space-y-6 md:space-y-0 md:space-x-6">
                <div className="flex flex-col w-full lg:mt-6">
                  <label htmlFor="email" className="block mb-1">
                    Email
                  </label>
                  <input
                    type="email"
                    id="email"
                    className="border shadow-md w-72 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
                <div className="flex flex-col w-full">
                  <label htmlFor="organization" className="block mb-1 lg:mt-6">
                    Organization
                  </label>
                  <input
                    type="text"
                    id="organization"
                    className="border shadow-md w-72 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                    onChange={(e) => setOrganization(e.target.value)}
                  />
                </div>
              </div>
              <div className="flex flex-col md:flex-row space-y-6 md:space-y-0 md:space-x-6">
                <div className="flex flex-col w-full">
                  <label htmlFor="password" className="block mb-1 lg:mt-6">
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    className="border shadow-md w-72 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
                <div className="flex flex-col w-full">
                  <label
                    htmlFor="confirmPassword"
                    className="block mb-1 lg:mt-6"
                  >
                    Confirm Password
                  </label>
                  <input
                    type="password"
                    id="confirmPassword"
                    className="border shadow-md w-72 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                    onChange={(e) => setConfirmPassword(e.target.value)}
                  />
                </div>
              </div>
              {/* Checkbox */}
              <div className="flex items-center">
                <input type="checkbox" id="rememberMe" className="mr-2" />
                <label htmlFor="rememberMe">Remember me</label>
              </div>
              {/* Sign Up button */}
              <div className="flex flex-col justify-center items-center">
                <button
                  onClick={handleSignup}
                  type="submit"
                  className="w-72 bg-gradient-to-r from-cyan-500 to-indigo-400 text-white py-3 rounded-md hover:bg-blue-600 transition duration-300 focus:outline-none focus:ring-2 focus:ring-gray-500"
                >
                  Create Account
                </button>
                {error && <p className="text-red-500 mt-2">{error}</p>}
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default SignupForm;
