import React, { useState } from "react";

// Component for the Add Patient button and form
const AddPatientButton = () => {
  // State to manage new patient data
  const [newPatient, setNewPatient] = useState({});
  // State to toggle form visibility
  const [showForm, setShowForm] = useState(false);

  // Function to add a new patient
  const addNewPatient = async () => {
    try {
      // Predefined therapist ID
      const therapistId = "65f116ab7989832f34d95ce6";
      // Create a new patient object including therapist ID
      const newPatientWithTherapistId = {
        ...newPatient,
        therapist_id: therapistId,
      };

      // Send POST request to the backend API
      const response = await fetch(
        "https://tele-backend.huelogics.com/add_patient",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(newPatientWithTherapistId),
        }
      );

      // Parse the response data
      const data = await response.json();
      console.log("New Patient Added:", data);

      // Fetch updated list of patients
      fetchPatients();
      // Hide the form after adding the patient
      setShowForm(false);
    } catch (error) {
      console.error("Error adding new patient:", error);
    }
  };

  // Function to handle input changes and update state
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    const newValue = name === "age" ? parseInt(value) : value;
    setNewPatient((prevPatient) => ({
      ...prevPatient,
      [name]: newValue,
    }));
  };

  return (
    <>
      {/* Desktop version */}
      <div>
        {/* Button to toggle the form visibility */}
        <button
          className="inline-flex mt-4 items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-500 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
          onClick={() => setShowForm(!showForm)}
        >
          Add Updated Patient
        </button>
        {showForm && (
          <div className="mt-4">
            <h2 className="text-xl font-semibold mb-2">Add New Patient</h2>
            <div className="grid grid-cols-1 gap-y-4 sm:grid-cols-2 sm:gap-x-4">
              <div className="col-span-1">
                <label className="block text-sm font-medium">Name</label>
                <input
                  type="text"
                  name="name"
                  onChange={handleInputChange}
                  className="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-800 text-white"
                />
              </div>
              <div className="col-span-1">
                <label className="block text-sm font-medium">Email</label>
                <input
                  type="email"
                  name="email"
                  onChange={handleInputChange}
                  className="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-800 text-white"
                />
              </div>
              <div className="col-span-1">
                <label className="block text-sm font-medium">Gender</label>
                <select
                  name="gender"
                  onChange={handleInputChange}
                  className="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-800 text-white"
                >
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </select>
              </div>
              <div className="col-span-1">
                <label className="block text-sm font-medium">Age</label>
                <input
                  type="number"
                  name="age"
                  onChange={handleInputChange}
                  className="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-800 text-white"
                />
              </div>
              <div className="col-span-2">
                <label className="block text-sm font-medium">Notes</label>
                <textarea
                  name="notes"
                  onChange={handleInputChange}
                  className="mt-1 focus:ring-blue-500 focus:border-blue-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-800 text-white"
                ></textarea>
              </div>
            </div>
            {/* Button to add a new patient */}
            <button
              onClick={addNewPatient}
              className="inline-flex mt-4 items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-500 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
            >
              Add Patient
            </button>
          </div>
        )}
      </div>
    </>
  );
};

export default AddPatientButton;
