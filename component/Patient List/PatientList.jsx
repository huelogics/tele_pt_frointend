import React, { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import ReactPaginate from "react-paginate";
import Image from "next/image";
import LeftNavbar from "../Navbar/LeftNavbar";
import NewNavbar from "../Navbar/NewNavbar";

const PatientList = () => {
  // State hooks for managing patient data, form visibility, search functionality, pagination, and drawer state
  const [patients, setPatients] = useState([]);
  const [newPatient, setNewPatient] = useState(null);
  const [showForm, setShowForm] = useState(false);
  const [filteredPatients, setFilteredPatients] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [pageNum, setPageNum] = useState(0);
  const [openDrawer, setOpenDrawer] = useState(false);

  // useEffect hook to fetch patients from the API when the component mounts
  useEffect(() => {
    const fetchPatients = async () => {
      try {
        const response = await axios.get(
          "https://tele-backend.huelogics.com/get_patients"
        );

        if (response) {
          setPatients(response.data);
        }
      } catch (error) {
        console.error("Error fetching patients:", error);
      }
    };
    fetchPatients();
  }, []);

  // Helper function to convert base64 string to an image URL
  const base64ToImage = (base64String) =>
    `data:image/png;base64,${base64String}`;

  // Function to handle search functionality
  const handleSearch = (query) => {
    const filtered = patients.filter((patient) =>
      patient.name.toLowerCase().includes(query.toLowerCase())
    );
    setFilteredPatients(filtered);
    setSearchQuery(query);
  };

  // Function to highlight the search query in the patient's name
  const highlightSearchQuery = (name) => {
    if (!searchQuery) return name;
    const regex = new RegExp(`(${searchQuery}), "gi")`);
    return name.replace(regex, "<mark>$1</mark>");
  };

  // Function to handle changes in the search input field
  const handleSearchInputChange = (e) => {
    setSearchQuery(e.target.value);
    //onSearch(e.target.value);
  };

  const router = useRouter();

  // Function to handle the Add Patient button click
  const onAddButton = async (e) => {
    router.push("/addpatient");
    query: {
      patientData: JSON.stringify(newPatient);
    }
  };

  // Function to handle clicking on a patient to view details
  const handlePatientClick = (patientId) => {
    router.push(`/patientdetails/${patientId}`);
  };

  // Function to handle logging out the user
  const handleLogout = async () => {
    try {
      await axios.post("https://tele-backend.huelogics.com/log_out");
      router.push("/");
    } catch (error) {
      if (error.isAxiosError && !error.response) {
        console.error("Network Error. Please check your internet connection.");
        router.push("/sign-up");
      } else {
        // Log the specific error response
        console.error("Error logging out:", error.response);
      }
    }
  };

  // Function to handle pagination click
  const handlePageClick = (selectedPage) => {
    setPageNum(selectedPage.selected);
  };

  // Calculate total pages for pagination
  const totalPage = Math.ceil(patients.length / 10);

  // Table header for displaying patient data
  const tableHeader = [
    "Patient Name",
    "Patient ID",
    "Therapist",
    "Therapist ID",
    "Date",
  ];

  return (
    <>
      {/* Main container */}
      <div className="w-full bg-[#6F86D642]  mr-4">
        <div className="flex gap-10 mr-7">
          {/* Left navigation bar */}
          <div className="h-screen mt-20">
            <div className="bgcolor rounded-full px-5 h-[20rem] top-20 ml-2">
              <div className="flex flex-col gap-2 my-auto py-6 ">
                <Image
                  src="/Sick2.png"
                  alt=""
                  width={30}
                  height={20}
                  style={{ cursor: "pointer" }}
                />
              </div>
            </div>
          </div>
          {/* Main content area */}
          <div className="mt-5 w-full">
            <div className="flex justify-between">
              <p className="font-sans text-2xl uppercase text-[#0060FF]">
                PATIENTS
              </p>
              {/* Search input */}
              <div className="relative">
                <div className="bg-white border border-gray-500 rounded-full w-96 px-2">
                  <input
                    type="text"
                    className="py-2 px-6 outline-none rounded-full"
                    placeholder="Search"
                    value={searchQuery}
                    onChange={handleSearchInputChange}
                  />
                </div>
                <div className="absolute top-2.5">
                  <Image
                    src="/Search.png"
                    alt=""
                    width={40}
                    height={30}
                    loading="lazy"
                  />
                </div>
              </div>
              {/* Icons for adding patients  */}
              <div className="flex gap-6 items-center">
                <svg
                  width="25"
                  height="23"
                  viewBox="0 0 25 23"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g filter="url(#filter0_d_1225_1015)">
                    <path
                      d="M24.4587 13.0413H14.2087V22.2913H10.792V13.0413H0.541992V9.95801H10.792V0.708008H14.2087V9.95801H24.4587V13.0413Z"
                      fill="#6F86D6"
                    />
                  </g>
                  <defs>
                    <filter
                      id="filter0_d_1225_1015"
                      x="0.541992"
                      y="0.708008"
                      width="23.9165"
                      height="21.583"
                      filterUnits="userSpaceOnUse"
                      color-interpolation-filters="sRGB"
                    >
                      <feFlood flood-opacity="0" result="BackgroundImageFix" />
                      <feColorMatrix
                        in="SourceAlpha"
                        type="matrix"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                      />
                      <feOffset />
                      <feComposite in2="hardAlpha" operator="out" />
                      <feColorMatrix
                        type="matrix"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
                      />
                      <feBlend
                        mode="normal"
                        in2="BackgroundImageFix"
                        result="effect1_dropShadow_1225_1015"
                      />
                      <feBlend
                        mode="normal"
                        in="SourceGraphic"
                        in2="effect1_dropShadow_1225_1015"
                        result="shape"
                      />
                    </filter>
                  </defs>
                </svg>

                <svg
                  width="40"
                  height="24"
                  viewBox="0 0 40 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M33.6 10.425C33.8562 9.85179 34 9.225 34 8.57143C34 5.73214 31.3125 3.42857 28 3.42857C26.7687 3.42857 25.6188 3.75 24.6688 4.29643C22.9375 1.725 19.7062 0 16 0C10.475 0 6 3.83571 6 8.57143C6 8.71607 6.00625 8.86071 6.0125 9.00536C2.5125 10.0607 0 12.9214 0 16.2857C0 20.5446 4.03125 24 9 24H32C36.4188 24 40 20.9304 40 17.1429C40 13.8268 37.25 11.0571 33.6 10.425ZM25.2938 15.1768L18.7062 20.8232C18.3187 21.1554 17.6813 21.1554 17.2938 20.8232L10.7063 15.1768C10.075 14.6357 10.525 13.7143 11.4125 13.7143H15.5V7.71429C15.5 7.24286 15.95 6.85714 16.5 6.85714H19.5C20.05 6.85714 20.5 7.24286 20.5 7.71429V13.7143H24.5875C25.475 13.7143 25.925 14.6357 25.2938 15.1768Z"
                    fill="#6F86D6"
                  />
                </svg>
              </div>
            </div>
            {/* Table to display patients */}
            <div className="mt-5 p-10 bg-white  shadow-2xl w-full rounded-3xl h-full">
              <table className="w-full">
                <thead>
                  <tr>
                    {tableHeader?.map((header, index) => (
                      <th
                        key={index}
                        className="text-[#0060FF] font-medium font-sans border-b border-gray-300"
                      >
                        {header}
                      </th>
                    ))}
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
        {/* Pagination component */}
        <div className="">
          <ReactPaginate
            pageCount={totalPage}
            onPageChange={handlePageClick}
            containerClassName="Pagination"
            activeClassName="active"
            previousLabel="Previous"
            marginPagesDisplayed={2}
            nextLabel="Next"
          />
        </div>
      </div>
      {/* Drawer for small screens */}
      <div className="lg:hidden block">
        {openDrawer && <Drawer setOpenDrawer={setOpenDrawer} />}
      </div>
    </>
  );
};

export default PatientList;
