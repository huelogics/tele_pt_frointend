import Image from "next/image";
import React from "react";
import Logo from "../../public/newlogo.svg";

function NewNavbar() {
  return (
    <div className="lg:mx-auto lg:px-5  bg-[#6F86D642]   w-full px-3  ">
      <div className=" lg:w-full lg:container lg:mx-auto lg:flex items-center lg:py-3 lg:px-14  w-[41.5rem]  flex justify-evenly">
        <Image src={Logo} alt="" width={100} height={100} loading="lazy" />
        <div className="lg:ml-auto flex lg:gap-2 items-center gap-6">
          <Image src={`/Rectangle.png`} width={40} height={40} loading="lazy" />
          <div className="flex flex-col items-center">
            <p className="text-gray-500 font-semibold text-base">Dr. Test</p>
            <p className="text-gray-500 font-medium text-xs">Therapist</p>
          </div>
          <div className="relative">
            <Image src={`/bell.png`} width={32} height={32} loading="lazy" />
            <div className="absolute top-0 right-0">
              <div className="px-1 py-px text-xs rounded-full bg-red-600">
                1
              </div>
            </div>
          </div>
          <div className="relative">
            <Image
              src={`/envelope1.png`}
              width={32}
              height={32}
              loading="lazy"
            />
            <div className="absolute top-0 right-0">
              <div className="px-1 py-px text-xs rounded-full bg-red-600">
                1
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NewNavbar;
