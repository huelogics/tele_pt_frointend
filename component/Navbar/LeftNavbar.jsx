import Image from "next/image";
import React from "react";
import { useRouter } from "next/router";

function LeftNavbar() {
  const router = useRouter();
  const handleClick = () => {
    router.push("/patient");
  };

  return (
    <div className="bgcolor  rounded-full px-5 h-96 ">
      <div className="flex flex-col gap-2 my-auto py-6 ">
        <Image
          src="/Sick2.png"
          alt=""
          width={30}
          height={20}
          style={{ cursor: "pointer" }}
          onClick={handleClick}
        />
      </div>
    </div>
  );
}

export default LeftNavbar;
