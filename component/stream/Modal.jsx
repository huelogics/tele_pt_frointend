import Image from "next/image";
import React from "react";

function Modal({ setShowModal }) {
  return (
    <div className="fixed z-50 inset-0 h-full w-full bg-opacity-30 bg-black flex justify-center items-center">
      <div className="w-2/4 h-2/4 overflow-y-auto bg-white p-5 rounded-lg">
        <div className="flex items-center">
          <h3 className="font-semibold font-sans text-lg">Records</h3>
          <Image
            onClick={() => setShowModal(false)}
            src="/cross.png"
            width={16}
            height={15}
            loading="lazy"
            className="ml-auto cursor-pointer"
          />
        </div>

        <div className="flex overflow-x-auto mt-4">
          <div>
            <table className="table-auto w-full border-collapse border border-gray-500">
              <thead>
                <tr className="bg-gray-200 lg:bg-[#0060FF] text-white">
                  <th className="border border-gray-500 px-4 py-2">
                    Procedure
                  </th>
                  <th className="border border-gray-500 px-4 py-2">Type</th>
                  <th className="border border-gray-500 px-4 py-2" colSpan="2">
                    Measurement
                  </th>
                </tr>
                <tr className="bg-gray-200 lg:bg-[#0060FF] text-white">
                  <th className="border border-gray-500 px-4 py-2"></th>
                  <th className="border border-gray-500 px-4 py-2"></th>
                  <th className="border border-gray-500 px-4 py-2">Start</th>
                  <th className="border border-gray-500 px-4 py-2">Stop</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Procedure"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Type"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Start"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Stop"
                    />
                  </td>
                </tr>
                <tr>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Procedure"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Type"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Start"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Stop"
                    />
                  </td>
                </tr>
                <tr>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Procedure"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Type"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Start"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Stop"
                    />
                  </td>
                </tr>
                <tr>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Procedure"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Type"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Start"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Stop"
                    />
                  </td>
                </tr>
                <tr>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Procedure"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Type"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Start"
                    />
                  </td>
                  <td className="border border-gray-500 px-4 py-2">
                    <input
                      type="text"
                      className="p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                      placeholder="Stop"
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;
