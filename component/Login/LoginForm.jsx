import React, { useState } from "react";
import SignupForm from "../Signup/SignUpForm";
import PatientList from "../Patient List/PatientList";
import { useRouter } from "next/router";
import axios from "axios";

const LoginForm = () => {
  // State hooks to manage component visibility and form inputs
  const [showSignup, setShowSignup] = useState(false);
  const [showLogin, setShowLogin] = useState(true);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [showSignupForm, setShowSignupForm] = useState(false);
  const router = useRouter();

  // Function to handle API call for login
  const loginApi = async () => {
    try {
      if (email && password) {
        const res = await axios.post(
          "https://tele-backend.huelogics.com/log_in",
          {
            email: email,
            password: password,
          }
        );
        if (res.status === 200) {
          // If login is successful, redirect to the patient page
          setShowComponent(true);
          router.push("/patient");
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  // Function to handle form submission
  const handleLogin = async (e) => {
    e.preventDefault();
    loginApi();
  };

  // Function to toggle the signup form
  const toggleSignup = () => {
    setShowSignup(!showSignup);
    setShowLogin(false);
    setShowSignupForm(true);

    const success = true;

    // If toggling signup is successful, redirect to the signup page
    if (success) {
      router.push("/sign-up");
    } else {
      setError("Login failed. Please check your credentials.");
    }
  };

  // If user is logged in, display the PatientList component
  if (isLoggedIn) {
    return <PatientList />;
  }

  return (
    <div className="grid lg:grid-cols-2 grid-cols-1 2xl:pl-32 lg:pl-5 gap-5">
      {/* Right side with gradient background */}
      <div className="container lg:order-first order-last mx-auto">
        <div className="flex-1 h-screen bg-white md:bg-blue-400 md:flex-1 lg:bg-white flex justify-center items-center">
          {showLogin && (
            <div className="p-8 w-full max-w-md text-black">
              <h2 className="text-2xl mb-20 text-center text-black font-semibold">
                Welcome
              </h2>
              {/* Login form */}
              <form className="space-y-4" onSubmit={handleLogin}>
                <div>
                  <label htmlFor="email" className="block mb-1">
                    Email/Phone number
                  </label>
                  <input
                    type="text"
                    id="email"
                    className="mt-2 shadow-md w-full border border-gray-300 rounded-md py-2 px-3 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                    placeholder="johndoe@example.com"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
                <div>
                  <label htmlFor="password" className="block mt-10">
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    className="mt-2 shadow-md w-full border border-gray-300 rounded-md py-2 px-3 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
                <div className=" ">
                  <a href="#" className="text-blue-300 hover:underline ">
                    Forgot password?
                  </a>
                </div>
                <div className="">
                  <button
                    onClick={() => {
                      router.push("/patient");
                    }}
                    type="submit"
                    className="w-full bg-gradient-to-r from-cyan-400 to-indigo-400 text-white py-2 rounded-md hover:bg-blue-600 transition duration-300 focus:outline-none focus:ring-2 focus:ring-gray-500"
                  >
                    Sign In
                  </button>
                </div>
              </form>
            </div>
          )}
        </div>
      </div>

      {/* Left side with gradient background */}
      <div className="flex-1 h-screen bg-gradient-to-r from-cyan-400 to-indigo-400 text-white relative flex flex-col justify-center items-center">
        <img
          src="/Logo.png"
          alt="Logo"
          className="absolute top-4 left-4 shadow-sm"
        />
        <div className="w-full p-8 flex flex-col justify-center items-center">
          <p className="text-xl mb-8 sm:mb-20 p-10 text-center">
            Don't Have an Account?
            <br />
            <span className="ml-0 ">Please Sign Up</span>
          </p>

          {/* Button to toggle signup form */}
          <button
            className="mt-8 px-10 py-4 w-40 font-semibold text-white border border-solid border-white hover:bg-white hover:text-black rounded-3xl"
            onClick={toggleSignup}
          >
            Sign Up
          </button>
        </div>
      </div>
    </div>
  );
};

export default LoginForm;
