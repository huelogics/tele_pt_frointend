export function Video({ video }) {
  return (
    <video width="320" height="240" controls preload="none">
      <source src={video} type="video/mp4" />
    </video>
  );
}
