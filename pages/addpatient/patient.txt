import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Drawer from "@/component/Drawer";

const AddPatientButton = ({
  setShowForm,
  handleInputChange,
  setNewPatient,
}) => {
  const [currentSection, setCurrentSection] = useState(1);
  const [imageUpload, setImageUpload] = useState(null);
  const router = useRouter();
  const totalSections = 2;
  const [openDrawer, setOpenDrawer] = useState(false);
  const handleNext = () => {
    setCurrentSection(currentSection + 1);
  };
  const [dob, setDob] = useState(null);
  const handleDateChange = (date) => {
    setDob(date);
    console.log("Selected Date:", date);
  };
  const addNewPatient = async () => {
    try {
      const therapistId = "65f116ab7989832f34d95ce6";
      const newPatientWithTherapistId = {
        ...newPatient,
        therapist_id: therapistId,
        dob: dob.toISOString(), // Convert date to ISO string
        photo: imageUpload, // Use the imageUpload state directly
      };

      const formData = new FormData();
      Object.entries(newPatientWithTherapistId).forEach(([key, value]) => {
        formData.append(key, value);
      });

      if (imageUpload) {
        formData.append("photo", imageUpload); // Use the imageUpload state directly
      }

      const response = await fetch(
        "https://tele-backend.huelogics.com/add_patient",
        {
          method: "POST",
          body: formData, // Send form data with image
        }
      );
      const data = await response.json();
      console.log("New Patient Added:", data);
      fetchPatients();
      setShowForm(false);
    } catch (error) {
      console.error("Error adding new patient:", error);
    }
  };

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setImageUpload(file);
  };

  useEffect(() => {
    const body = document.querySelector("body");
    body.style.overflow = openDrawer ? "hidden" : "auto";
  });

  return (
    <>
      {/* Desktop Version*/}
      <div className="container hidden lg:flex font-sans">
        {/* Left Section */}
        <div className="lg:min-h-screen lg:bg-gradient-to-r from-cyan-500 to-indigo-400  lg:w-64 p-4  justify-center items-center sticky ">
          <div className="mt-0 mb-32 h-16 w-24 ml-16">
            <img src="/Logo.png" alt="Logo" className="" />
            <p className="text-white text-sm">HUE LOGICS</p>
          </div>

          <div className="">
            <button className=" lg:ml-4 lg:inline-flex items-center px-8 py-2 mt-14 lg:border border-white lg:rounded-md lg:shadow-xl lg:text-lg  lg:font-medium lg:text-white lg:bg-gradient-to-r from-cyan-500 to-indigo-400  hover:bg-blue-600 lg:focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
              <img
                src="/Sick.png"
                alt="Patient"
                className="lg:h-6 lg:w-11 lg:mr-2"
                onClick={() => router.back()}
              />
              Patient
            </button>
          </div>
        </div>
        <div className="lg:flex-auto p-8 lg:bg-white border border-gray-300 text-black">
          {/* Navbar */}
          <div className="p-4 lg:flex items-center justify-between  border-b  mb-10 border-gray-300">
            <img
              src="/Back.png"
              alt="Back"
              className="h-6 w-6 mr-4"
              onClick={() => router.back()}
            />
            <div className="flex items-center ml-auto">
              <img src="/Rectangle.png" alt="Image" className="h-6 w-6 mx-2" />
              <p>
                Dr. Test
                <br />
                <span>Therepist</span>
              </p>
              <img src="/Doorbell.png" alt="Bell" className="h-6 w-6 mx-2" />
              <img src="/Envelope.png" alt="Email" className="h-6 w-6 mx-2" />
            </div>
          </div>
          <h1 className="text-4xl mb-6 ">Personal Information</h1>
          {/* FORM */}
          {currentSection === 1 && (
            <div className="flex flex-col items-center justify-center">
              <div className="grid grid-cols-1  gap-y-5 sm:grid-cols-2 gap-x-9 border border-slate-400 shadow-xl p-14 items-center">
                <div className="col-span-1">
                  <label className="block text-sm font-medium">Name</label>
                  <input
                    type="text"
                    name="name"
                    onChange={handleInputChange}
                    className="mt-1 border shadow-md w-96 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  />
                </div>
                <div className="col-span-1">
                  <label className="block text-sm font-medium">Email</label>
                  <input
                    type="email"
                    name="email"
                    onChange={handleInputChange}
                    className="mt-1 border shadow-md w-96 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  />
                </div>
                <div className="col-span-1">
                  <label className="block text-sm font-medium">
                    Date of Birth
                  </label>
                  <DatePicker
                    selected={dob}
                    onChange={handleDateChange}
                    dateFormat="yyyy-MM-dd"
                    calendarClassName="custom-calendar"
                    className="mt-1 border shadow-md w-96 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  />
                </div>
                <div className="col-span-1">
                  <label className="block text-sm font-medium">Gender</label>
                  <select
                    name="gender"
                    onChange={handleInputChange}
                    className="mt-1 border shadow-md w-96 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  >
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
                </div>
                <div className="col-span-1">
                  <label className="block text-sm font-medium">Age</label>
                  <input
                    type="age"
                    name="age"
                    onChange={handleInputChange}
                    className="mt-1 border shadow-md w-96 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  />
                </div>

                <div className="col-span-1">
                  <label className="block text-sm font-medium">
                    Bloodgroup
                  </label>
                  <input
                    type="text"
                    name="name"
                    onChange={handleInputChange}
                    className="mt-1 border shadow-md w-96 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  />
                </div>
                <div className="col-span-1">
                  <label className="block text-sm font-medium">Height</label>
                  <input
                    type="text"
                    name="name"
                    onChange={handleInputChange}
                    className="mt-1 border shadow-md w-96 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  />
                </div>
                <div className="col-span-1">
                  <label className="block text-sm font-medium">Weight</label>
                  <input
                    type="text"
                    name="name"
                    onChange={handleInputChange}
                    className="mt-1 border shadow-md w-96 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  />
                </div>
                <div className="col-span-1">
                  <label className="flex  text-sm font-medium">
                    Upload Images
                  </label>
                  <label
                    htmlFor=""
                    className="relative cursor-pointer bg-white border shadow-md w-96 border-gray-300 rounded-md py-3 px-4 text-center focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  >
                    <input
                      id="file-upload"
                      type="file"
                      onChange={handleFileChange}
                    />
                  </label>
                </div>
                <div className="mt-8 col-span-2 flex justify-center">
                  <button
                    onClick={handleNext}
                    className="inline-flex items-center px-8 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gradient-to-r from-cyan-500 to-indigo-400  hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                  >
                    Save
                  </button>
                </div>
              </div>
              <div className="flex justify-center mt-8">
                <button
                  onClick={handleNext}
                  className="ml-10 inline-flex items-center px-8 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gradient-to-r from-cyan-500 to-indigo-400  hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 mr-4"
                >
                  Next
                </button>
              </div>
            </div>
          )}
          {currentSection === 2 && (
            <div className="flex justify-center">
              <div className="mt-1 p-4 h-fit w-[50rem] border shadow-md  flex flex-col justify-center items-center border-gray-400 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400">
                <label className="text-lg font-semibold ">History</label>
                <textarea
                  name="notes"
                  onChange={handleInputChange}
                  className="p-2 w-full h-[30rem] flex justify-center items-center"
                  placeholder="Write a note.."
                ></textarea>
                <div className="w-full flex justify-center items-center mt-8">
                  {currentSection > 1 && (
                    <div className="w-full flex justify-center">
                      <button
                        onClick={() => setCurrentSection(currentSection - 1)}
                        className="px-8 py-2 border border-transparent rounded-full shadow-sm text-sm font-medium text-white bg-gradient-to-r from-cyan-500 to-indigo-400 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                      >
                        Previous
                      </button>
                    </div>
                  )}
                </div>
              </div>
            </div>
          )}

          <div className="w-full mt-4  flex justify-center">
            {currentSection === totalSections && (
              <button
                onClick={addNewPatient}
                className="inline-flex items-center px-10 py-2 border border-transparent rounded-full shadow-sm text-sm font-medium text-white bg-gradient-to-r from-cyan-500 to-indigo-400 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
              >
                Save
              </button>
            )}
          </div>
        </div>
      </div>

      {/* Mobile version */}
      <div className=" lg:hidden container font-sans  px-5 my-5">
        <div className="flex flex-col gap-5">
          {/* Navbar */}
          <div className="flex items-center">
            <div
              onClick={() => setOpenDrawer(true)}
              className="border border-gray-400 rounded-lg p-3"
            >
              <img src="/hamburger.png" alt="" width={20} height={20} />
            </div>
            <div className="ml-auto">
              <div className="flex items-center ml-auto">
                <img
                  src="/Rectangle.png"
                  alt="Image"
                  className="h-6 w-6 mx-2"
                />
                <p className="text-sm">Dr. Test Therapist</p>
              </div>
            </div>
          </div>

          {/* FORM */}
          <h1 className="text-xl flex font-bold  ">Personal Information</h1>
          {currentSection === 1 && (
            <div className="flex flex-col   items-center justify-center">
              <div className=" border border-slate-200 shadow-xl p-8   text-start items-center">
                <div className="input-container">
                  <input
                    type="text"
                    name="name"
                    onChange={handleInputChange}
                    className="input-field"
                  />
                  <label htmlFor="name" className="placeholder-label">
                    Name
                  </label>
                </div>

                <div className="input-container mt-4">
                  <input
                    type="email"
                    name="name"
                    onChange={handleInputChange}
                    className="input-field"
                  />
                  <label htmlFor="name" className="placeholder-label">
                    Email
                  </label>
                </div>

                <div className="input-container mt-4">
                  <input
                    type="date"
                    name="name"
                    onChange={handleInputChange}
                    className="input-field"
                  />
                  <label htmlFor="name" className="placeholder-label">
                    Date
                  </label>
                </div>

                <div className="input-container mt-4">
                  <select
                    name="gender"
                    onChange={handleInputChange}
                    className=" border shadow-md  border-gray-300 rounded-md w-full py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  >
                    <option value="Male">Select Gender</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
                </div>

                <div className="input-container mt-4">
                  <input
                    type="text"
                    name="name"
                    onChange={handleInputChange}
                    className="input-field"
                  />
                  <label htmlFor="name" className="placeholder-label">
                    Age
                  </label>
                </div>

                <div className="input-container mt-4">
                  <input
                    type="text"
                    name="name"
                    onChange={handleInputChange}
                    className="input-field"
                  />
                  <label htmlFor="name" className="placeholder-label">
                    Bloodgroup
                  </label>
                </div>

                <div className="input-container mt-4">
                  <input
                    type="text"
                    name="name"
                    onChange={handleInputChange}
                    className="input-field"
                  />
                  <label htmlFor="name" className="placeholder-label">
                    Height
                  </label>
                </div>

                <div className="input-container mt-4">
                  <input
                    type="text"
                    name="name"
                    onChange={handleInputChange}
                    className="input-field"
                  />
                  <label htmlFor="name" className="placeholder-label">
                    Weight
                  </label>
                </div>

                <div className="col-span-1">
                  <label className="block text-sm font-medium">Photo</label>
                  <input
                    type="file"
                    name="photo"
                    onChange={handleFileChange}
                    className="mt-1 border shadow-md w-96 border-gray-300 rounded-md py-3 px-4 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400"
                  />
                </div>
                <div className="  flex justify-center items-center  ">
                  <button
                    onClick={handleNext}
                    className="mt-6  inline-flex items-center px-8 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gradient-to-r from-cyan-500 to-indigo-400  hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 mr-4"
                  >
                    Next
                  </button>
                </div>
              </div>
              <div className="flex justify-center items-center mt-10">
                <button
                  onClick={handleNext}
                  className="inline-flex items-center px-8 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gradient-to-r from-cyan-500 to-indigo-400  hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                >
                  Save
                </button>
              </div>
            </div>
          )}
          {currentSection === 2 && (
            <div className="">
              <div className="">
                <div className="mt-1  p-2 h-fit w-full   border shadow-xl  border-gray-400 focus:outline-none focus:border-gray-400 focus:ring-1 focus:ring-gray-400">
                  {" "}
                  <label className="text-lg font-bold ">History</label>
                  <textarea
                    name="notes"
                    onChange={handleInputChange}
                    className=" p-2 w-full h-[15rem]"
                    placeholder="Write a note.."
                  ></textarea>
                  {currentSection > 1 && (
                    <div className=" flex justify-center ">
                      <button
                        onClick={() => setCurrentSection(currentSection - 1)}
                        className=" flex justify-center px-8 py-2 mt-14 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gradient-to-r from-cyan-500 to-indigo-400  hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                      >
                        Previous
                      </button>
                    </div>
                  )}
                </div>
              </div>
            </div>
          )}
          <div className="flex justify-center mt-2">
            {currentSection === totalSections && (
              <div className=" flex justify-center ">
                <button
                  onClick={addNewPatient}
                  className="inline-flex items-center px-8 py-2 mt-14 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-gradient-to-r from-cyan-500 to-indigo-400  hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                >
                  Save
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
      {openDrawer && <Drawer setOpenDrawer={setOpenDrawer} />}
    </>
  );
};

export default AddPatientButton;
