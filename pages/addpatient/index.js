import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import LeftNavbar from "@/component/Navbar/LeftNavbar";
import NewNavbar from "@/component/Navbar/NewNavbar";
import Image from "next/image";
// Component for adding a new patient
const AddPatientButton = ({
  setShowForm,
  handleInputChange,
  setNewPatient,
}) => {
  const [currentSection, setCurrentSection] = useState(1); // Current section of the form
  const [imageUpload, setImageUpload] = useState(null); // Uploaded image
  const router = useRouter(); // Next.js router
  const totalSections = 2; // Total sections in the form

  const [openDrawer, setOpenDrawer] = useState(false); // State for drawer open/close

  const handleNext = () => {
    setCurrentSection(currentSection + 1);
  };
  const [dob, setDob] = useState(null); // Date of birth of the patient
  const handleDateChange = (date) => {
    setDob(date);
    console.log("Selected Date:", date);
  };
  // Function to add a new patient
  const addNewPatient = async () => {
    try {
      const therapistId = "65f116ab7989832f34d95ce6";
      const newPatientWithTherapistId = {
        ...newPatient,
        therapist_id: therapistId,
        dob: dob.toISOString(),
        photo: imageUpload,
      };
      // Creating form data to send to server
      const formData = new FormData();
      Object.entries(newPatientWithTherapistId).forEach(([key, value]) => {
        formData.append(key, value);
      });
      // Appending image file to form data if uploaded
      if (imageUpload) {
        formData.append("photo", imageUpload);
      }
      // Sending POST request to add new patient
      const response = await fetch(
        "https://tele-backend.huelogics.com/add_patient",
        {
          method: "POST",
          body: formData,
        }
      );
      // Getting response data
      const data = await response.json();
      console.log("New Patient Added:", data);
      // Fetching updated list of patients
      fetchPatients();
      // Closing the form
      setShowForm(false);
    } catch (error) {
      console.error("Error adding new patient:", error);
    }
  };
  // Function to handle file change (uploading image)
  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setImageUpload(file);
  };
  // Effect hook to control body overflow based on drawer state
  useEffect(() => {
    const body = document.querySelector("body");
    body.style.overflow = openDrawer ? "hidden" : "auto";
  });
  // State variables and functions for managing multi-page form
  const [currentPage, setCurrentPage] = useState(1);

  const handleNextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, 3));
  };

  const handlePreviousPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };
  // State variables and functions for managing medical information
  const [medication, setMedication] = useState("");
  const [medicalCondition, setMedicalCondition] = useState("");
  const [accidentHistory, setAccidentHistory] = useState([
    { text: "", date: "" },
  ]);
  const [surgeryHistory, setSurgeryHistory] = useState([
    { text: "", date: "", file: null },
  ]);

  const handleMedicationChange = (e) => {
    setMedication(e.target.value);
  };

  const handleMedicalConditionChange = (e) => {
    setMedicalCondition(e.target.value);
  };

  const handleAddAccidentHistory = (e) => {
    e.preventDefault();
    setAccidentHistory([...accidentHistory, { text: "", date: "" }]);
  };

  const handleAccidentHistoryChange = (index, field, value) => {
    const newHistory = accidentHistory.map((history, i) =>
      i === index ? { ...history, [field]: value } : history
    );
    setAccidentHistory(newHistory);
  };

  const handleAddSurgeryHistory = (e) => {
    e.preventDefault();
    setSurgeryHistory([...surgeryHistory, { text: "", date: "", file: null }]);
  };

  const handleSurgeryHistoryChange = (index, field, value) => {
    const newHistory = surgeryHistory.map((history, i) =>
      i === index ? { ...history, [field]: value } : history
    );
    setSurgeryHistory(newHistory);
  };

  // Arrays for body parts and pain types
  const bodyParts = ["Front", "Back", "Left Side", "Right Side"];
  const painTypes = [
    "Deep",
    "Superficial",
    "Burning",
    "Sharp",
    "Throbbing",
    "Shooting",
    "Stinging",
  ];

  return (
    <>
      {/* <div className="container mx-auto bg-[#aee8fb]  px-5 ">
        <NewNavbar />
        <div className="flex gap-10 ">
          <div className="mt-20">
            <LeftNavbar onClick={handleNavClick} />
          </div>

          <div className=" mt-2 w-full">
            <div className=" justify-center items-center">
              <div className="mt-2 ">
                <div className="">
                  <div className="grid grid-cols-6 gap-2 justify-between mb-4">
                    <div className="rounded-full h-8 w-8 flex items-center justify-center bg-blue-700 text-white font-bold">
                      1
                    </div>
                    <hr className="border border-black col-span-1 my-auto" />
                    <div className="rounded-full h-8 w-8 flex items-center justify-center bg-blue-700 text-white font-bold">
                      2
                    </div>
                    <hr className="border border-black col-span-1 my-auto" />
                    <div className="rounded-full h-8 w-8 flex items-center justify-center bg-blue-700 text-white font-bold">
                      3
                    </div>
                  </div>
                  <div className="grid grid-cols-3 gap-2 justify-between">
                    <div>
                      <h2 className="text-lg font-bold mb-2">
                        Personal Information
                      </h2>
                    </div>
                    <div>
                      <h2 className="text-lg font-bold mb-2">
                        Contact Information
                      </h2>
                    </div>
                    <div>
                      <h2 className="text-lg font-bold mb-2">History</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="max-w-full mx-auto p-6 bg-white shadow-md rounded-md">
              <form>
                {" "}
                <div className="mb-4 grid grid-cols-2 gap-10">
                  <label className="block text-gray-700  tracking-wider">
                    Patient Name
                  </label>
                </div>{" "}
                <div className="mb-4 grid grid-cols-2 gap-10">
                  <input
                    type="text"
                    className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                    placeholder="First Name"
                  />
                  <input
                    type="text"
                    className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                    placeholder="Surname"
                  />
                </div>
                <div className="mb-4 grid grid-cols-2 gap-10">
                  <label className="block text-gray-700  tracking-wider">
                    DOB
                  </label>
                  <label className="block text-gray-700  tracking-wider">
                    Age
                  </label>
                </div>
                <div className="mb-4 grid grid-cols-2 gap-10">
                  <input
                    type="date"
                    className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                  />

                  <input
                    type="number"
                    className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                  />
                </div>
                <div className="mb-4 grid grid-cols-2 gap-10">
                  <label className="block text-gray-700  tracking-wider">
                    Gender
                  </label>
                  <label className="block text-gray-700  tracking-wider">
                    Photo
                  </label>
                </div>
                <div className="mb-4 grid grid-cols-2 gap-10">
                  <select className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300">
                    <option value="">Select Gender</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="other">Other</option>
                  </select>
                  <input
                    type="file"
                    className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                  />
                </div>
                <div className="mb-4 grid grid-cols-2 gap-10">
                  <label className="block text-gray-700 tracking-wider">
                    Height
                  </label>
                  <label className="block text-gray-700"> </label>
                </div>
                <div className="mb-4 grid grid-cols-2 gap-10">
                  <input
                    type="text"
                    className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                  />
                </div>
                <div className="mb-4 grid grid-cols-2 gap-10">
                  <label className="block text-gray-700  tracking-wider">
                    Weight
                  </label>
                  <label className="block text-gray-700"></label>
                </div>
                <div className="mb-4 grid grid-cols-2 gap-10">
                  <input
                    type="text"
                    className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                  />
                </div>
                <div className="mb-4">
                  <button
                    type="submit"
                    className="px-4 py-2 bg-[#aee8fb] text-white rounded-md hover:bg-green-600 focus:outline-none focus:ring focus:border-blue-300"
                  >
                    SAVE
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> */}

      <div className=" mx-auto h-screen  px-5">
        <div className="flex flex-col md:flex-row gap-10">
          <div className="mt-20">
            <LeftNavbar />
          </div>

          <div className="mt-2 w-full">
            <div className="justify-center items-center">
              <div className="mt-2">
                <div className="">
                  <div className="grid grid-cols-5 gap-3 justify-between mb-4">
                    <div
                      className={`flex items-center justify-center ${
                        currentPage >= 1 ? "bg-blue-700" : "bg-gray-300"
                      } rounded-full h-8 w-8`}
                    >
                      <div
                        className={`flex items-center justify-center h-6 w-6 rounded-full ${
                          currentPage >= 1
                            ? "bg-blue-700 text-white"
                            : "bg-gray-300 text-gray-700"
                        } border-2 border-white font-bold`}
                      >
                        1
                      </div>
                    </div>
                    <hr className="border border-black col-span-1 my-auto" />
                    <div
                      className={`flex items-center justify-center ${
                        currentPage >= 2 ? "bg-blue-700" : "bg-gray-300"
                      } rounded-full h-8 w-8`}
                    >
                      <div
                        className={`flex items-center justify-center h-6 w-6 rounded-full ${
                          currentPage >= 2
                            ? "bg-blue-700 text-white"
                            : "bg-gray-300 text-gray-700"
                        } border-2 border-white font-bold`}
                      >
                        2
                      </div>
                    </div>
                    <hr className="border border-black col-span-1 my-auto" />
                    <div
                      className={`flex items-center justify-center ${
                        currentPage >= 3 ? "bg-blue-700" : "bg-gray-300"
                      } rounded-full h-8 w-8`}
                    >
                      <div
                        className={`flex items-center justify-center h-6 w-6 rounded-full ${
                          currentPage >= 3
                            ? "bg-blue-700 text-white"
                            : "bg-gray-300 text-gray-700"
                        } border-2 border-white font-bold`}
                      >
                        3
                      </div>
                    </div>
                  </div>

                  <div className="grid grid-cols-1 sm:grid-cols-3 gap-2 justify-between">
                    <div>
                      <h2 className="text-lg font-bold mb-2">
                        Personal Information
                      </h2>
                    </div>
                    <div>
                      <h2 className="text-lg font-bold mb-2">
                        Contact Information
                      </h2>
                    </div>
                    <div>
                      <h2 className="text-lg font-bold mb-2">History</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="max-w-full mx-auto p-6 bg-white shadow-md rounded-md">
              <form>
                {currentPage === 1 && (
                  <div>
                    <div className="mb-4 grid grid-cols-1 sm:grid-cols-2 gap-10">
                      <label className="block text-gray-700 tracking-wider">
                        Patient Name
                      </label>
                    </div>
                    <div className="mb-4 grid grid-cols-1 sm:grid-cols-2 gap-10">
                      <input
                        type="text"
                        className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                        placeholder="First Name"
                      />
                      <input
                        type="text"
                        className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                        placeholder="Surname"
                      />
                    </div>
                    <div className="mb-4 grid grid-cols-1 sm:grid-cols-2 gap-10">
                      <label className="block text-gray-700 tracking-wider">
                        DOB
                      </label>
                      <label className="block text-gray-700 tracking-wider">
                        Age
                      </label>
                    </div>
                    <div className="mb-4 grid grid-cols-1 sm:grid-cols-2 gap-10">
                      <input
                        type="date"
                        className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                      />
                      <input
                        type="number"
                        className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                      />
                    </div>
                    <div className="mb-4 grid grid-cols-1 sm:grid-cols-2 gap-10">
                      <label className="block text-gray-700 tracking-wider">
                        Gender
                      </label>
                      <label className="block text-gray-700 tracking-wider">
                        Photo
                      </label>
                    </div>
                    <div className="mb-4 grid grid-cols-1 sm:grid-cols-2 gap-10">
                      <select className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300">
                        <option value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="other">Other</option>
                      </select>
                      <input
                        type="file"
                        className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                      />
                    </div>
                    <div className="mb-4 grid grid-cols-1 sm:grid-cols-2 gap-10">
                      <label className="block text-gray-700 tracking-wider">
                        Weight
                      </label>
                      <label className="block text-gray-700 tracking-wider">
                        Height
                      </label>
                    </div>
                    <div className="mb-4 grid grid-cols-1 sm:grid-cols-2 gap-10">
                      <input
                        type="text"
                        className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                      />
                      <input
                        type="text"
                        className="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                      />
                    </div>
                  </div>
                )}

                {currentPage === 2 && (
                  <div>
                    <div className="mb-4 sm:grid-cols-2 gap-10">
                      <div className="mb-4 grid grid-cols-4 items-center">
                        <label className="block text-gray-700 tracking-wider">
                          Are you currently taking any medication?
                        </label>
                        <div className="grid grid-cols-4 mt-2">
                          <label className="mr-4">
                            <input
                              type="radio"
                              name="medication"
                              value="yes"
                              className="mr-1"
                              onChange={handleMedicationChange}
                            />
                            Yes
                          </label>
                          <label className="ml-10">
                            <input
                              type="radio"
                              name="medication"
                              value="no"
                              className="mr-1"
                              onChange={handleMedicationChange}
                            />
                            No
                          </label>
                        </div>
                      </div>
                      {medication === "yes" && (
                        <div class="flex space-x-1 space-y-1 ">
                          <div class="">
                            <button class="mt-2  w-10 h-10 rounded-full bg-black text-white  justify-center  text-2xl">
                              +
                            </button>
                          </div>

                          <input
                            type="text"
                            class="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                            placeholder="Enter text"
                          />
                        </div>
                      )}
                      <div className="mb-4 mt-5 grid grid-cols-3 items-center">
                        <label className="block text-gray-700 tracking-wider">
                          Are you ever diagnosed with any medical condition?
                        </label>
                        <div className="grid grid-cols-5 mt-2">
                          <label className="mr-4">
                            <input
                              type="radio"
                              name="medicalCondition"
                              value="yes"
                              className="mr-1"
                              onChange={handleMedicalConditionChange}
                            />
                            Yes
                          </label>
                          <label className="ml-10">
                            <input
                              type="radio"
                              name="medicalCondition"
                              value="no"
                              className="mr-1"
                              onChange={handleMedicalConditionChange}
                            />
                            No
                          </label>
                        </div>
                      </div>
                      {medicalCondition === "yes" && (
                        <div class="flex space-x-1 space-y-1 mt-2">
                          <div class="">
                            <button class="mt-2  w-10 h-10 rounded-full bg-black text-white  justify-center  text-2xl">
                              +
                            </button>
                          </div>

                          <input
                            type="text"
                            class="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                            placeholder="Enter text"
                          />
                        </div>
                      )}
                      <div className="mb-4 mt-6">
                        <label className="block text-gray-700 tracking-wider">
                          Accident History: (Slips/Falls/Sports related
                          injuries/Auto accidents)
                        </label>
                        {accidentHistory.map((history, index) => (
                          <div
                            key={index}
                            class="flex space-x-1 space-y-1 mt-2"
                          >
                            <div class="">
                              <button class="mt-2 w-10 h-10 rounded-full bg-black text-white justify-center  text-2xl">
                                +
                              </button>
                            </div>

                            <input
                              type="text"
                              class="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                              placeholder="Enter text"
                              value={history.text}
                            />

                            <input
                              type="date"
                              class="w-32 px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                            />
                          </div>
                        ))}
                      </div>
                      <div className="mb-4">
                        <label className="mt-5 block text-gray-700 tracking-wider">
                          Surgery History:
                        </label>
                        {surgeryHistory.map((history, index) => (
                          <div
                            key={index}
                            class="flex space-x-1 space-y-1 mt-2"
                          >
                            <div class="">
                              <button class="w-10 h-10 rounded-full bg-black text-white justify-center mt-2 text-2xl">
                                +
                              </button>
                            </div>

                            <input
                              type="text"
                              class="w-full px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                              placeholder="Enter text"
                              value={history.text}
                            />

                            <input
                              type="date"
                              class="w-32 px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                            />
                            <input
                              type="file"
                              class=" px-3 py-2 border rounded-md focus:outline-none focus:ring focus:border-blue-300"
                            />
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                )}

                {currentPage === 3 && (
                  <div className="flex ">
                    <div>
                      <Image src="/body.png" alt="" width={1300} height={100} />
                    </div>
                    <div className="flex flex-col items-center">
                      <div
                        style={{
                          background:
                            "linear-gradient(90deg, #667EEA 0%, #764BA2 100%)",
                          width: "352px",
                          height: "99px",
                          top: "250px",
                          left: "1088px",
                          gap: "0px",
                          borderRadius: "30px 0px 0px 30px",
                          opacity: "0px",
                        }}
                      >
                        <div className="flex flex-col justify-center items-center space-x-2 space-y-2">
                          <h1 className=" mt-3 text-white text-center">
                            Locate the symptoms
                          </h1>
                          <p className=" text-white text-center">
                            Please mark your area(s) of pain
                          </p>
                        </div>
                      </div>

                      <div className="flex flex-col mt-2">
                        <label className="block text-gray-700 tracking-wider">
                          Describe the symptoms:
                        </label>
                        <label className="ml-10">
                          <input
                            type="radio"
                            name="medicalCondition"
                            value="no"
                            className="mr-1"
                          />
                          Deep
                        </label>
                        <label className="ml-10">
                          <input
                            type="radio"
                            name="medicalCondition"
                            value="no"
                            className="mr-1"
                          />
                          Superficial
                        </label>
                        <label className="ml-10">
                          <input
                            type="radio"
                            name="medicalCondition"
                            value="no"
                            className="mr-1"
                          />
                          Burning
                        </label>
                        <label className="ml-10">
                          <input
                            type="radio"
                            name="medicalCondition"
                            value="no"
                            className="mr-1"
                          />
                          Throbbing
                        </label>
                        <label className="ml-10">
                          <input
                            type="radio"
                            name="medicalCondition"
                            value="no"
                            className="mr-1"
                          />
                          Sharp
                        </label>
                        <label className="ml-10">
                          <input
                            type="radio"
                            name="medicalCondition"
                            value="no"
                            className="mr-1"
                          />
                          Shooting
                        </label>
                        <label className="ml-10">
                          <input
                            type="radio"
                            name="medicalCondition"
                            value="no"
                            className="mr-1"
                          />
                          Stinging
                        </label>
                      </div>

                      <div className="flex flex-col mt-2">
                        <label className="block text-gray-700 tracking-wider">
                          Describe the symptoms:
                        </label>
                        <label className="ml-10">
                          <input
                            type="radio"
                            name="medicalCondition"
                            value="no"
                            className="mr-1"
                          />
                          Deep
                        </label>
                        <label className="ml-10">
                          <input
                            type="radio"
                            name="medicalCondition"
                            value="no"
                            className="mr-1"
                          />
                          Superficial
                        </label>
                        <label className="ml-10">
                          <input
                            type="radio"
                            name="medicalCondition"
                            value="no"
                            className="mr-1"
                          />
                          Burning
                        </label>
                      </div>
                    </div>
                  </div>
                )}

                <div className="flex justify-between">
                  <button
                    type="button"
                    onClick={handlePreviousPage}
                    disabled={currentPage === 1}
                    className="px-4 py-2 bg-gray-300 text-white rounded-md hover:bg-gray-400 focus:outline-none focus:ring focus:border-blue-300"
                  >
                    Previous
                  </button>
                  {currentPage < 3 && (
                    <button
                      type="button"
                      onClick={handleNextPage}
                      className="px-4 py-2 bg-blue-700 text-white rounded-md hover:bg-blue-800 focus:outline-none focus:ring focus:border-blue-300"
                    >
                      Next
                    </button>
                  )}
                  {currentPage === 3 && (
                    <button
                      type="submit"
                      className="px-4 py-2 bg-[#aee8fb] text-white rounded-md hover:bg-green-600 focus:outline-none focus:ring focus:border-blue-300"
                    >
                      SAVE
                    </button>
                  )}
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddPatientButton;
