import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import Image from "next/image";
import Drawer from "@/component/Drawer";

const PatientDetails = () => {
  const router = useRouter();
  const { patientId } = router.query;
  const [patientDetails, setPatientDetails] = useState(null);
  const [patientRecords, setPatientRecords] = useState([]);
  const [openDrawer, setOpenDrawer] = useState(false);

  useEffect(() => {
    const fetchPatientDetails = async () => {
      try {
        const [patientResponse, recordsResponse] = await Promise.all([
          axios.get(
           ` https://tele-backend.huelogics.com/get_patient/${patientId}`
          ),
          axios.get(
           ` https://tele-backend.huelogics.com/get_records/${patientId}  `
          ),
        ]);

        if (patientResponse) {
          setPatientDetails(patientResponse.data);
        }

        if (recordsResponse) {
          setPatientRecords(recordsResponse.data);
        }
      } catch (error) {
        console.error("Error fetching patient details:", error);
      }
    };
    if (patientId) {
      fetchPatientDetails();
    }
  }, [patientId]);

  useEffect(() => {
    const body = document.querySelector("body");
    body.style.overflow = openDrawer ? "hidden" : "auto";
  });

  if (!patientDetails) {
    return <div className="container mx-auto p-8">Loading...</div>;
  }

  const handleViewRecord = async (recordId) => {
    try {
      const response = await axios.get(
       ` https://tele-backend.huelogics.com/get_record/${recordId}`
      );
      if (response) {
        const recordDetails = response.data;
        router.push({
          pathname: "/recorddetails",
          query: {
            recordId: recordDetails.record_id,
            patientId: patientId,
          },
        });
      }
    } catch (error) {
      console.error("Error fetching record details:", error);
    }
    console.log(recordId);
  };

  const base64ToImage = (base64String) => {
    return `data:image/png;base64,${base64String}`;
  };

  const handleNewRecord = () => {
    router.push("/stream");
  };

  return (
    <>
      {/* Desktop version */}
      <div className=" container lg:flex hidden font-sans">
        {/* Left Section */}
        <div className="bg-gradient-to-r from-cyan-500 to-indigo-400 w-64 p-4  justify-center items-center  ">
          <div className="mt-0 mb-32 h-16 w-24 ml-16">
            <img src="/Logo.png" alt="Logo" className="" />
            <p className="text-white text-sm">HUE LOGICS</p>
          </div>

          <div className="">
            <button className="ml-4 inline-flex items-center px-8 py-2 mt-14 border border-white rounded-md shadow-xl text-lg  font-medium  text-white  bg-gradient-to-r from-cyan-500 to-indigo-400 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
              <img
                src="/Sick.png"
                alt="Patient"
                className="h-6 w-11 mr-2"
                onClick={() => router.back()}
              />
              Patient
            </button>
          </div>
        </div>

        {/* Right Section */}
        <div className="flex-auto p-8 bg-white border border-gray-300 text-black shadow-xl">
          {/* Navbar */}
          <div className="p-4 flex items-center justify-between md:justify-start border-b border-gray-300 sticky top-0 bg-white">
            <img
              src="/Back.png"
              alt="Back"
              className="h-6 w-6 mr-4"
              onClick={() => router.back()}
            />
            <div className="flex items-center ml-auto">
              <img src="/Rectangle.png" alt="Image" className="h-6 w-6 mx-2" />

              <p className="text-sm">
                Dr. Test
                <br />
                <span className="text-sm">Therapist</span>
              </p>
              <img src="/Doorbell.png" alt="Bell" className="h-6 w-7 mx-2" />
              <img src="/Envelope.png" alt="Email" className="h-6 w-7 mx-2" />
            </div>
          </div>

          {/* Button New Mesurements */}
          <div className="flex justify-between items-center mt-4">
            <h1 className="text-2xl font-bold">PATIENT</h1>
            <div className="flex space-x-4 ">
              {" "}
              <button
                //onClick={onAddButton}
                className="inline-flex items-center px-4 py-3 border border-transparent rounded-full shadow-sm text-sm font-medium  bg-gradient-to-r from-cyan-500 to-indigo-400 text-white hover:bg-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 "
              >
                <img src="/Downloads.png" alt="add" className="mr-2" />
                Download{" "}
              </button>
              <button
                onClick={handleNewRecord}
                className="inline-flex items-center px-4 py-3 border border-transparent rounded-full shadow-sm text-sm font-medium text-white  bg-gradient-to-r from-cyan-500 to-indigo-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
              >
                <img src={"/AddNew.png"} alt="Add" className="mr-2" />
                New Record
              </button>
            </div>
          </div>

          {/* Patient Detail Box */}

          <div className="flex items-center mt-5">
            <div>
              <Image
                src={base64ToImage(patientDetails.photo)}
                alt=""
                width={1000}
                height={1000}
                className="rounded-2xl w-56 h-64 object-cover"
              />
            </div>
            <table className=" border w-full h-56 border-gray-500">
              <thead>
                <tr className="border-b-2 ">
                  <td className="border-l-2 px-5 py-2">
                    <div className="flex flex-col">
                      <strong>Name</strong>
                      <p>{patientDetails.name}</p>
                    </div>
                  </td>
                  <td className="border-l-2 px-5 py-2">
                    <div className="flex flex-col">
                      <strong>Email</strong>
                      <p>{patientDetails.email}</p>
                    </div>
                  </td>
                  <td className="border-l-2 px-5 py-2">
                    <div className="flex flex-col">
                      <strong>Gender</strong>
                      <p>{patientDetails.gender}</p>
                    </div>
                  </td>
                  <td className="border-l-2 px-5 py-2">
                    <div className="flex flex-col">
                      <strong>Age</strong>
                      <p>{patientDetails.age}</p>
                    </div>
                  </td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="px-5 py-2 text-black border-l-2">
                    <strong>History</strong>
                  </td>
                  <td className="p-2 text-black">{patientDetails.notes}</td>
                </tr>
              </tbody>
            </table>
          </div>

          {/* <div className="container mx-auto p-8 ">
          <div className="flex flex-col ">
            <div className="w-full mb-4  flex">
              <div className="relative mb-4 mr-4">
                <img
                  src={base64ToImage(patientDetails.photo)}
                  alt="Patient Photo"
                  className="rounded-2xl w-56 h-64 object-cover"
                />
              </div>
              <div className="p-4 ml-1">
                <table className=" border w-full h-56 border-gray-500">
                  <thead>
                    <tr className="border-b-2">
                      <td className="border-l-2 p-5">
                        <div className="flex flex-col gap-2 ">
                          <strong>Name</strong>
                          <p>{patientDetails.name}</p>
                        </div>
                      </td>
                      <td className="border-l-2 p-5">
                        <div className="flex flex-col gap-2 ">
                          <strong>Email</strong>
                          <p>{patientDetails.email}</p>
                        </div>
                      </td>
                      <td className="border-l-2 p-5">
                        <div className="flex flex-col gap-2 ">
                          <strong>Gender</strong>
                          <p>{patientDetails.gender}</p>
                        </div>
                      </td>
                      <td className="border-l-2 p-5">
                        <div className="flex flex-col gap-2 ">
                          <strong>Age</strong>
                          <p>{patientDetails.age}</p>
                        </div>
                      </td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="p-5 text-black border-l-2">
                        <strong>History</strong>
                      </td>
                      <td className="p-2 text-black">{patientDetails.notes}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div> */}

          <div className="flex justify-center mt-20 px-10">
            <table className="border border-gray-400 w-full ">
              <thead>
                <tr className="  bg-gradient-to-b to-[#48C6EF] from-[#6F86D6]  text-white">
                  <th className="border border-black px-4 py-2  ">Record</th>
                  <th className="border border-black px-4 py-2">Therapist</th>
                  <th className="border border-black px-4 py-2">
                    Therapist ID
                  </th>
                  <th className="border border-gray-400 px-4 py-2">Date</th>
                </tr>
              </thead>
              <tbody>
                <tr className="bg-white text-gray-700">
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                </tr>
                <tr className="bg-white text-gray-700">
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      {/* Mobile version */}

      <div className="block lg:hidden container mx-auto px-5 my-5">
        <div className="flex flex-col gap-5">
          {/* Navbar */}
          <div className="flex items-center">
            <div
              onClick={() => setOpenDrawer(true)}
              className="border border-gray-400 rounded-lg p-3"
            >
              <Image src="/hamburger.png" alt="" width={20} height={20} />
            </div>
            <div className="ml-auto">
              <div className="flex items-center ml-auto">
                <img
                  src="/Rectangle.png"
                  alt="Image"
                  className="h-6 w-6 mx-2"
                />

                <p className="text-sm">
                  Dr. Test Therapist
                  {/* <br />
                <span className="text-sm"></span> */}
                </p>
                {/* <img src="/Doorbell.png" alt="Bell" className="h-6 w-7 mx-2" />
              <img src="/Envelope.png" alt="Email" className="h-6 w-7 mx-2" /> */}
              </div>
            </div>
          </div>

          <div className="bg-gray-300 h-px w-full"></div>
          <h1 className=" font-bold text-2xl text-center">PATIENT</h1>

          <div className="grid grid-cols-2 gap-2 ">
            <button className="inline-flex py-2 items-center justify-center rounded-full shadow-sm text-sm font-medium  bg-gradient-to-r from-cyan-500 to-indigo-400 text-white hover:bg-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 ">
              {/* <img src="/Downloads.png" alt="add" /> */}
              <Image src="/Downloads.png" width={20} height={20} alt="add" />
              Download{" "}
            </button>
            <button
              onClick={handleNewRecord}
              className="inline-flex items-center  justify-center rounded-full shadow-sm text-sm font-medium text-white  bg-gradient-to-r from-cyan-500 to-indigo-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
            >
              <Image src={"/AddNew.png"} alt="Add" width={20} height={20} />
              {/* <img src={"/AddNew.png"} alt="Add" /> */}
              New Record
            </button>
          </div>

          <div>
            <Image
              src={base64ToImage(patientDetails.photo)}
              alt=""
              width={1000}
              height={1000}
              className="rounded-2xl w-full h-64 object-cover"
            />
          </div>
          <div className="flex items-center mt-5 overflow-x-scroll hideScroller">
            <table className=" border w-full h-56 border-gray-500">
              <thead>
                <tr className="border-b-2 ">
                  <td className="border-l-2 px-5 py-2">
                    <div className="flex flex-col">
                      <strong>Name</strong>
                      <p>{patientDetails.name}</p>
                    </div>
                  </td>
                  <td className="border-l-2 px-5 py-2">
                    <div className="flex flex-col">
                      <strong>Email</strong>
                      <p>{patientDetails.email}</p>
                    </div>
                  </td>
                  <td className="border-l-2 px-5 py-2">
                    <div className="flex flex-col">
                      <strong>Gender</strong>
                      <p>{patientDetails.gender}</p>
                    </div>
                  </td>
                  <td className="border-l-2 px-5 py-2">
                    <div className="flex flex-col">
                      <strong>Age</strong>
                      <p>{patientDetails.age}</p>
                    </div>
                  </td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="px-5 py-2 text-black border-l-2">
                    <strong>History</strong>
                  </td>
                  <td className="p-2 text-black">{patientDetails.notes}</td>
                </tr>
              </tbody>
            </table>
          </div>

          <div className="flex items-center overflow-x-scroll hideScroller">
            {/* <table className="border border-gray-400 w-full ">
              <thead>
                <tr className="  bg-gradient-to-b to-[#48C6EF] from-[#6F86D6]  text-white">
                  <th className="border border-black px-4 py-2  ">Record</th>
                  <th className="border border-black px-4 py-2">Therapist</th>
                  <th className="border border-black px-4 py-2">
                    Therapist ID
                  </th>
                  <th className="border border-gray-400 px-4 py-2">Date</th>
                </tr>
              </thead>
              <tbody>
                <tr className="bg-white text-gray-700">
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                </tr>
                <tr className="bg-white text-gray-700">
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                </tr>
              </tbody>
            </table> */}

            <table className="w-full">
              <thead>
                <tr className="  bg-gradient-to-b to-[#48C6EF] from-[#6F86D6]  text-white">
                  <td className="border border-black px-4 py-2">Record</td>
                  <td className="border border-black px-4 py-2">Therapist</td>
                  <td className="border border-black px-4 py-2">
                    Therapist ID
                  </td>
                  <td className="border border-black px-4 py-2">Date</td>
                </tr>
              </thead>
              <tbody>
                <tr className="bg-white text-gray-700">
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                </tr>
                <tr className="bg-white text-gray-700">
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                  <td className="border border-black px-4 py-2">-</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {openDrawer && <Drawer setOpenDrawer={setOpenDrawer} />}
    </>

    // <div className="w-full md:w-1/2 flex justify-end">
    // <div className="grid grid-cols-2 gap-4">
    //   {patientRecords.map((record, index) => (
    //     <div key={index} className=" p-4 rounded-lg">
    //       <p className="text-black">
    //         <strong>Therapist Name:</strong> {record.therapist_name}
    //       </p>
    //       <p className="text-black">
    //         <strong>Date:</strong>{" "}
    //         {new Date(record.date).toLocaleDateString()}
    //       </p>
    //       {/* Add more details here as needed */}
    //     </div>
    //   ))}
    // </div>
    // </div>

    // <div className="container mx-auto p-8 bg-gray-800 rounded-lg shadow-lg">
    //
    //     <h1 className="text-3xl font-bold text-white mb-4">Patient Details</h1>
    //     <div className="flex flex-col md:flex-row md:justify-between">
    //         <div className="w-full md:w-1/2 mb-4 md:mb-0">
    //             <p className="text-white mb-2"><strong>Name:</strong> {patientDetails.name}</p>
    //             <p className="text-white mb-2"><strong>Email:</strong> {patientDetails.email}</p>
    //             <p className="text-white mb-2"><strong>Gender:</strong> {patientDetails.gender}</p>
    //             <p className="text-white mb-2"><strong>Age:</strong> {patientDetails.age}</p>
    //             <p className="text-white mb-2"><strong>Notes:</strong> {patientDetails.notes}</p>

    //         </div>
    //         <div className="w-full md:w-1/2">

    //             <p className="text-white mb-2"><strong>Photo:</strong> <img src={base64ToImage(patientDetails.photo)} alt="Patient Photo" /></p>
    //         </div>
    //     </div>

    //     <ul className="mt-8">
    //         {patientRecords.map((record, index) => (
    //             <li key={index} className="border-b border-gray-600 py-4">
    //                 <div className="flex flex-col md:flex-row md:justify-between">
    //                     <div className="w-full md:w-1/2 mb-4 md:mb-0">
    //                         <p className="text-white mb-2"><strong>Therapist Name:</strong> {record.therapist_name}</p>
    //                         <p className="text-white mb-2"><strong>Date:</strong> {new Date(record.date).toLocaleDateString()}</p>
    //                     </div>
    //
    //                 </div>
    //             </li>
    //         ))}
    //     </ul>
    //
    // </div>
  );
};

export default PatientDetails;