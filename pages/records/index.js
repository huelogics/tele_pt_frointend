import React, { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router";
import Image from "next/image";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { EffectCoverflow, Navigation, Pagination } from "swiper/modules";

const Records = () => {
  const [showIcons, setShowIcons] = useState(false);
  const popUpRef = useRef(null);
  const [centerSlideIndex, setCenterSlideIndex] = useState(0);
  const swiperRef = useRef(null); // Add a ref for the Swiper instance

  useEffect(() => {
    if (showIcons) {
      document.addEventListener("mousedown", handleClickOutside);
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [showIcons]);

  const handleClickOutside = (event) => {
    if (popUpRef.current && !popUpRef.current.contains(event.target)) {
      setShowIcons(false);
    }
  };

  const router = useRouter();
  const handleClick = () => {
    router.push("/patient");
  };

  const images = [
    "/Rectangle 53.png",
    "/Rectangle 53.png",
    "/Rectangle 53.png",
    "/Rectangle 53.png",
    "/Rectangle 53.png",
    "/Rectangle 53.png",
    "/Rectangle 53.png",
    "/Rectangle 53.png",
  ];

  const handleNextClick = () => {
    if (swiperRef.current && swiperRef.current.swiper) {
      swiperRef.current.swiper.slideNext();
    }
  };

  const handlePrevClick = () => {
    if (swiperRef.current && swiperRef.current.swiper) {
      swiperRef.current.swiper.slidePrev();
    }
  };

  return (
    <div className="mx-auto">
      {/* Navbar */}
      <div className="lg:w-full text-black ">
        <div className="lg:p-4 flex sticky top-0 gap-5 items-center right-0 md:justify-start border-b border-gray-200 bg-white">
          <div className="flex items-center ml-auto px-4 py-4 w-fit rounded-full gap-10">
            <div className="flex gap-2 items-center">
              <img
                src="/Rectangle.png"
                alt="Image"
                width={40}
                height={40}
                loading="lazy"
              />
              <div className="flex flex-col items-center text-gray-500">
                <span className="font-bold text-lg">Dr Test</span>
                <p className="text-xs">Therapist</p>
              </div>
            </div>
          </div>
          <div className="flex items-center relative">
            <img src={`/bell.png`} width={32} height={32} loading="lazy" />
            <div className="absolute right-0">
              <div className="mb-4 rounded-full bg-red-600 text-white h-4 w-4 text-center">
                <h1 className="text-xs">1</h1>
              </div>
            </div>
          </div>
          <div className="flex items-center relative">
            <img src={`/envelope1.png`} width={32} height={32} loading="lazy" />
            <div className="absolute right-0">
              <div className="mb-4 rounded-full bg-red-600 text-white h-4 w-4 text-center">
                <h1 className="text-xs">1</h1>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="flex ">
        <div className="mt-10 mr-10 ml-5">
          <div className="bgcolor  rounded-full px-5 h-[30rem]  fixed top-13 left-1 ">
            <div className="flex flex-col gap-2 my-auto py-6 ">
              <Image
                src="/Sick2.png"
                alt=""
                width={30}
                height={20}
                style={{ cursor: "pointer" }}
                onClick={handleClick}
              />
            </div>
          </div>
        </div>
        <div className="w-full overflow-hidden">
          <div className="flex gap-[80rem] justify-items-stretch">
            <div>
              <p className="tracking-wider text-2xl font-bold ml-6 mt-3 text-[#6F86D6] uppercase">
                RECORD 1
              </p>
            </div>
          </div>

          <div className="flex mb-10 ml-10 gap-10 mt-10">
            <div>
              <h1 className=" text-xl font-bold  mb-5">Records</h1>
              <table className="rounded-xl border-separate  shadow-2xl border-[#6F86D6] border-2">
                <thead>
                  <tr className="text-gray-300 rounded-3xl">
                    <th className="border border-gray-200 px-4 py-2">
                      Procedure
                    </th>
                    <th className="border border-gray-200 px-4 py-2">
                      Options
                    </th>
                    <th className="border border-gray-200 px-4 py-2">
                      CV Reading
                    </th>
                    <th
                      className="border border-gray-200 px-4 py-2"
                      colSpan="2"
                    >
                      Physical Reading
                    </th>
                  </tr>
                  <tr className="bg-white text-gray-300">
                    <th className="border border-gray-200 px-4 py-2"></th>
                    <th className="border border-gray-200 px-4 py-2"></th>
                    <th className="border border-gray-200 px-4 py-2">Start</th>
                    <th className="border border-gray-200 px-4 py-2">Stop</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Procedure"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Type"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Start"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none block w-full sm:text-sm rounded-md"
                        placeholder="Stop"
                      />
                    </td>
                  </tr>
                  <tr>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Procedure"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Type"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Start"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none block w-full sm:text-sm rounded-md"
                        placeholder="Stop"
                      />
                    </td>
                  </tr>
                  <tr>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Procedure"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Type"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Start"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none block w-full sm:text-sm rounded-md"
                        placeholder="Stop"
                      />
                    </td>
                  </tr>
                  <tr>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Procedure"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Type"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm rounded-md"
                        placeholder="Start"
                      />
                    </td>
                    <td className="border border-gray-200 px-4 py-2">
                      <input
                        type="text"
                        className="p-3 outline-none block w-full sm:text-sm rounded-md"
                        placeholder="Stop"
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div>
              <h1 className="text-xl font-bold mb-5">Notes</h1>
              <div className="rounded-xl  p-3 relative shadow-xl border-[#6F86D6] border-2">
                <textarea
                  className="w-96 h-[20rem] cursor-text   resize-none outline-none bg-transparent text-base"
                  placeholder="Notes..."
                ></textarea>
                <div className="absolute top-0 left-0 w-full h-full pointer-events-none"></div>
              </div>
            </div>
          </div>
          <div className="container mb-10 ">
            <div className="bg-[#716FD6] p-2 rounded-t-3xl shadow-xl mr-10 max-w-full ml-10">
              <div className="relative overflow-x-hidden">
                <div className=" px-2 lg:px-0 lg:mt-20 mt-13">
                  <div className="lg:mt-16 mt-8 ">
                    <Swiper
                      ref={swiperRef} // Assign the ref to Swiper
                      autoplay={{
                        delay: 5500,
                        disableOnInteraction: false,
                      }}
                      effect={"coverflow"}
                      grabCursor={true}
                      centeredSlides={true}
                      coverflowEffect={{
                        rotate: 0,
                        stretch: 0,
                        depth: 90,
                        modifier: 2.5,
                      }}
                      loop={true}
                      navigation={{
                        nextEl: ".swiper-button-next",
                        prevEl: ".swiper-button-prev",
                      }}
                      style={{
                        "--swiper-navigation-color": "#ffffff",
                        "--swiper-navigation-hover-color": "#fffff",
                      }}
                      modules={[EffectCoverflow, Navigation, Pagination]}
                      onSwiper={(swiper) =>
                        setCenterSlideIndex(swiper.activeIndex)
                      }
                      className="homepagemoments "
                      breakpoints={{
                        500: {
                          slidesPerView: 2,
                        },
                        768: {
                          slidesPerView: 3,
                        },
                        1024: {
                          slidesPerView: 4,
                        },
                      }}
                    >
                      {images.map((image, index) => (
                        <SwiperSlide key={index}>
                          <div className="swiper-slide gap-10">
                            <Image
                              src={image}
                              alt={`Image ${index + 1}`}
                              width={400}
                              height={300}
                            />
                          </div>
                        </SwiperSlide>
                      ))}
                      <div
                        className="swiper-button-next"
                        onClick={handleNextClick}
                      ></div>
                      <div
                        className="swiper-button-prev"
                        onClick={handlePrevClick}
                      ></div>
                    </Swiper>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Records;
