// Importing the PatientList component
import PatientList from "@/component/Patient List/PatientList";
import React from "react";

// Functional component representing the index page
const Index = () => {
  return (
    <div>
      {/* Rendering the PatientList component */}
      <PatientList />
    </div>
  );
};

export default Index;
