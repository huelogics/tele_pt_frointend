import LeftNavbar from "@/component/Navbar/LeftNavbar";
import Image from "next/image";
import React from "react";
import { useRouter } from "next/router";

function ViewPatient() {
  const router = useRouter();

  const handleClick = () => {
    router.push("/patient");
  };
  return (
    <>
      <div className=" min-h-screen">
        <div className="lg:mx-auto lg:px-5 bg-[#6F86D642]     w-full px-3  ">
          <div className="sticky top-0 z-50 lg:w-full lg:container lg:mx-auto lg:flex items-center lg:py-3 lg:px-14  w-[41.5rem]  flex justify-evenly">
            <div className="lg:ml-auto flex lg:gap-2 items-center gap-6">
              <Image
                src={`/Rectangle.png`}
                width={40}
                height={40}
                loading="lazy"
              />
              <div className="flex flex-col items-center">
                <p className="text-gray-500 font-semibold text-base">
                  Dr. Test
                </p>
                <p className="text-gray-500 font-medium text-xs">Therapist</p>
              </div>
              <div className="relative">
                <Image
                  src={`/bell.png`}
                  width={32}
                  height={32}
                  loading="lazy"
                />
                <div className="absolute top-0 right-0">
                  <div className="px-1 py-px text-xs rounded-full bg-red-600">
                    1
                  </div>
                </div>
              </div>
              <div className="relative">
                <Image
                  src={`/envelope1.png`}
                  width={32}
                  height={32}
                  loading="lazy"
                />
                <div className="absolute top-0 right-0">
                  <div className="px-1 py-px text-xs rounded-full bg-red-600">
                    1
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className=" mx-auto px-5 flex font-sans bg-[#6F86D642]">
          <div className="flex gap-10">
            <div className="mt-20">
              <div className="bgcolor  rounded-full px-5 h-[30rem]  fixed top-13 left-1 ">
                <div className="flex flex-col gap-2 my-auto py-6 ">
                  <Image
                    src="/Sick2.png"
                    alt=""
                    width={30}
                    height={20}
                    style={{ cursor: "pointer" }}
                    onClick={handleClick}
                  />
                </div>
              </div>
            </div>
            {/* Main Content */}
            <div className="w-3/4 mt-5  ">
              <div className="flex justify-between">
                <h1 className="text-[#6F86D6] uppercase ml-10 tracking-wider">
                  Patient Overview
                </h1>
              </div>
              <div className="mt-5 ml-10 text-center  p-2 bg-white rounded-xl w-[300px] h-[230px] shadow-xl">
                <div className="flex flex-col justify-center items-center ">
                  <Image
                    src={`/Rectangle.png`}
                    width={40}
                    height={90}
                    loading="lazy"
                    className=""
                  />
                  <div className="font-semibold text-xl text-[#6F86D6]">
                    Patient
                  </div>
                  <div className="font-normal  text-sm mt-0">Male</div>
                </div>

                <div className="grid grid-cols-2  gap-10 items-center">
                  <div className="mt-4">
                    {" "}
                    <div className="font-semibold text-[#6F86D6]">Age</div>
                    <div className="text-sm">29</div>
                    <div className="font-semibold text-[#6F86D6] mt-4">
                      Blood
                    </div>
                    <div className="text-sm">B+</div>
                  </div>

                  <div className="mt-4">
                    <div className="font-semibold text-[#6F86D6] ">Weight</div>
                    <div className="text-sm">55kg</div>
                    <div className="font-semibold text-[#6F86D6] mt-4">
                      Height
                    </div>
                    <div className="text-sm">159cm</div>
                  </div>
                </div>
              </div>
              <div className="mt-5 p-5 ml-10 rounded-lg bg-white shadow-lg w-[300px] h-[400px] ">
                <h2 className="text-xl font-bold mb-4 text-[#6F86D6]  border-b border-gray-300 ">
                  Past Treatments
                </h2>
                <div className="flex justify-between items-center mb-2">
                  <div className="font-bold">Leg Injury</div>
                  <div className="text-[#6F86D6] cursor-pointer">View</div>
                </div>
                <div className="text-gray-400 text-xs font-semibold">
                  22 September 2020
                </div>
              </div>
            </div>
          </div>

          {/* Right Side */}
          <div className="w-full ml-20  ">
            <div className="mt-5 flex flex-col space-y-5 bg-white p-6 shadow-xl rounded-xl">
              <div className="bg-white p-5">
                <div className="flex justify-end gap-6 items-center">
                  <svg
                    width="40"
                    height="34"
                    viewBox="0 0 33 34"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    onClick={() => router.push("/addpatient")}
                    style={{ cursor: "pointer" }}
                  >
                    <g filter="url(#filter0_d_1196_799)">
                      <path
                        d="M28.4583 14.75H18.2083V25.25H14.7916V14.75H4.54163V11.25H14.7916V0.75H18.2083V11.25H28.4583V14.75Z"
                        fill="#6F86D6"
                      />
                    </g>
                    <defs>
                      <filter
                        id="filter0_d_1196_799"
                        x="0.541626"
                        y="0.75"
                        width="31.9166"
                        height="32.5"
                        filterUnits="userSpaceOnUse"
                        color-interpolation-filters="sRGB"
                      >
                        <feFlood
                          flood-opacity="0"
                          result="BackgroundImageFix"
                        />
                        <feColorMatrix
                          in="SourceAlpha"
                          type="matrix"
                          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                          result="hardAlpha"
                        />
                        <feOffset dy="4" />
                        <feGaussianBlur stdDeviation="2" />
                        <feComposite in2="hardAlpha" operator="out" />
                        <feColorMatrix
                          type="matrix"
                          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
                        />
                        <feBlend
                          mode="normal"
                          in2="BackgroundImageFix"
                          result="effect1_dropShadow_1196_799"
                        />
                        <feBlend
                          mode="normal"
                          in="SourceGraphic"
                          in2="effect1_dropShadow_1196_799"
                          result="shape"
                        />
                      </filter>
                    </defs>
                  </svg>

                  <svg
                    width="40"
                    height="24"
                    viewBox="0 0 40 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M33.6 10.425C33.8562 9.85179 34 9.225 34 8.57143C34 5.73214 31.3125 3.42857 28 3.42857C26.7687 3.42857 25.6188 3.75 24.6688 4.29643C22.9375 1.725 19.7062 0 16 0C10.475 0 6 3.83571 6 8.57143C6 8.71607 6.00625 8.86071 6.0125 9.00536C2.5125 10.0607 0 12.9214 0 16.2857C0 20.5446 4.03125 24 9 24H32C36.4188 24 40 20.9304 40 17.1429C40 13.8268 37.25 11.0571 33.6 10.425ZM25.2938 15.1768L18.7062 20.8232C18.3187 21.1554 17.6813 21.1554 17.2938 20.8232L10.7063 15.1768C10.075 14.6357 10.525 13.7143 11.4125 13.7143H15.5V7.71429C15.5 7.24286 15.95 6.85714 16.5 6.85714H19.5C20.05 6.85714 20.5 7.24286 20.5 7.71429V13.7143H24.5875C25.475 13.7143 25.925 14.6357 25.2938 15.1768Z"
                      fill="#6F86D6"
                    />
                  </svg>
                </div>
              </div>
              <div className="flex gap-10 justify-evenly">
                {/* Blood Sugar */}
                <div className="p-5 shadow-xl shadow-blue-300 flex-1  rounded-3xl">
                  <div className="flex items-center">
                    <Image
                      src="/3.png"
                      alt=""
                      width={40}
                      height={30}
                      loading="lazy"
                      className="bg-[#6F86D642] p-2 rounded-xl mr-2 w-11"
                    />
                    <h2 className="text-base font-bold mb-2 tracking-wider">
                      Blood Sugar
                    </h2>
                  </div>
                  <div className="mt-6">
                    <div className="flex mt-2 items-center space-x-2">
                      <div className="font-normal text-4xl">80</div>
                      <div className="text-gray-500 font-bold">mg / dL</div>
                    </div>
                    <div className="mt-2 font-normal bg-[#6F86D642] w-fit px-3 py-2 rounded-xl text-base">
                      Normal
                    </div>
                  </div>
                </div>
                {/* Heart Rate */}
                <div className="p-5 shadow-xl shadow-blue-300 flex-1 rounded-3xl">
                  <div className="flex items-center">
                    <Image
                      src="/1.png"
                      alt=""
                      width={40}
                      height={80}
                      loading="lazy"
                      className="bg-[#6F86D642] p-2 rounded-xl mr-2 w-16"
                    />
                    <h2 className="text-base font-bold mb-2 tracking-wider">
                      Heart Rate
                    </h2>
                  </div>
                  <div className="mt-6">
                    <div className="flex items-center space-x-2">
                      <div className="font-normal text-4xl">98</div>
                      <div className="text-gray-500 font-bold">bpm</div>
                    </div>
                    <div className="mt-2 font-normal bg-[#6F86D642] w-fit px-3 py-2 rounded-xl text-base">
                      Normal
                    </div>
                  </div>
                </div>
                {/* Blood Pressure */}
                <div className="p-5 shadow-xl shadow-blue-300 flex-1  rounded-3xl ">
                  <div className="flex items-center">
                    <Image
                      src="/2.png"
                      alt=""
                      width={40}
                      height={30}
                      loading="lazy"
                      className="bg-[#6F86D642] p-2 rounded-xl mr-2 w-11"
                    />
                    <h2 className="text-base font-bold mb-2 tracking-wider">
                      Blood Pressure
                    </h2>
                  </div>
                  <div className="mt-6">
                    <div className="flex items-center space-x-2">
                      <div className="font-normal text-4xl">102</div>
                      <div>/</div>
                      <div className="font-bold text-gray-500">72</div>
                      <div className="text-gray-500 font-bold">mmhg</div>
                    </div>
                    <div className="mt-2 font-normal bg-[#6F86D642] w-fit px-3 py-2 rounded-xl text-base">
                      Normal
                    </div>
                  </div>
                </div>
              </div>
              {/* Record */}
              <div>
                <div className=" p-5 shadow-2xl shadow-blue-300 rounded-3xl   mt-20 ">
                  <h2 className="text-xl font-bold mb-2 text-[#6F86D6]">
                    Record
                  </h2>
                  <table className="min-w-full leading-normal">
                    <thead>
                      <tr>
                        <th className="px-5 py-3 border-b-2  text-left text-xs font-semibold text-gray-400 uppercase tracking-wider">
                          Record
                        </th>
                        <th className="px-5 py-3 border-b-2  text-left text-xs font-semibold text-gray-400 uppercase tracking-wider">
                          Therapist
                        </th>
                        <th className="px-5 py-3 border-b-2  text-left text-xs font-semibold text-gray-400 uppercase tracking-wider">
                          Therapist ID
                        </th>
                        <th className="px-5 py-3 border-b-2  text-left text-xs font-semibold text-gray-400 uppercase tracking-wider">
                          Date
                        </th>
                      </tr>
                    </thead>
                    <tbody className="text-white">
                      <tr>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          John Doe
                        </td>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          John Doe
                        </td>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          #12345
                        </td>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          May 20, 2024
                        </td>
                      </tr>
                      <tr>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          John Doe
                        </td>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          John Doe
                        </td>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          #12345
                        </td>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          May 20, 2024
                        </td>
                      </tr>
                      <tr>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          John Doe
                        </td>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          John Doe
                        </td>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          #12345
                        </td>
                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          May 20, 2024
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ViewPatient;
