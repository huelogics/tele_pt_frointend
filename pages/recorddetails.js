import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import { Video } from "@/component/Video";

const RecordDetails = () => {
  const router = useRouter();
  const { recordId } = router.query;
  const [recordDetails, setRecordDetails] = useState(null);
  const [processedVideo, setProcessedVideo] = useState(null);

  useEffect(() => {
    const fetchRecordDetails = async () => {
      try {
        const response = await axios.get(
          `https://tele-backend.huelogics.com/get_record/${recordId}`
        );
        if (response) {
          setRecordDetails(response.data);
        }
      } catch (error) {
        console.error("Error fetching record details:", error);
      }
    };
    if (recordId) {
      fetchRecordDetails();
    }
  }, [recordId]);

  const handleUpload = async () => {
    try {
      const response = await axios.post(
        `https://tele-backend.huelogics.com/process_video`,
        { recordId }
      );
      if (response && response.data && response.data.processed_video) {
        setProcessedVideo(response.data.processed_video);
      }
    } catch (error) {
      console.error("Error processing video:", error);
    }
  };

  if (!recordDetails) {
    return <div className=" container mx-auto p-8 ">Loading...</div>;
  }

  return (
    <div className="container mx-auto bg-gray-900 text-white p-8 rounded-lg shadow-lg">
      <button
        onClick={() => router.back()}
        className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 mb-5 rounded focus:outline-none focus:shadow-outline"
      >
        Go Back
      </button>

      <div className="flex flex-col md:flex-row justify-between items-start md:items-end">
        <h1 className="text-3xl font-bold mb-4">Record Details </h1>
        <p className="mb-4 text-right">
          Date:{" "}
          {recordDetails.date
            ? new Date(recordDetails.date).toLocaleDateString()
            : ""}
        </p>
      </div>
      <h1 className="text-xl  mb-4">Record ID : {recordDetails.record_id} </h1>

      <div className="mb-8">
        {/* <h2 className="text-white text-2xl font-bold mb-4">Processed Videos</h2> */}
        <div className="grid grid-cols-1 md:grid-cols-2 gap-0 ">
          <iframe
            title="Processed Video 1"
            width="50%"
            height="315"
            src="/Video/UnProcessed.mp4"
            allowFullScreen
            className="rounded-lg"
          />
          <iframe
            title="Processed Video 2"
            width="50%"
            height="315"
            src="/Video/Processed.mp4"
            allowFullScreen
            className="rounded-lg"
          />
        </div>
      </div>

      <div>
        <h2 className="text-2xl font-bold mb-4">Measurements</h2>
        <table className="table-auto w-full">
          <thead>
            <tr className="bg-gray-800">
              <th className="border border-gray-700 px-4 py-2">Procedure</th>
              <th className="border border-gray-700 px-4 py-2">Option</th>
              <th className="border border-gray-700 px-4 py-2">
                Computer Vision Reading
              </th>
              <th className="border border-gray-700 px-4 py-2">
                Physical Reading
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="border border-gray-700 px-4 py-2">
                Elbow Flexion
              </td>
              <td className="border border-gray-700 px-4 py-2">Left Arm</td>
              <td className="border border-gray-700 px-4 py-2">52 degrees</td>
              <td className="border border-gray-700 px-4 py-2">51 degrees</td>
            </tr>
          </tbody>
        </table>
      </div>
      <p className="mt-8">
        <strong>Notes: </strong> Range of motion for left arm elbow flexion is
        normal
      </p>
    </div>
  );
};

export default RecordDetails;
