// Importing the SignupForm component
import SignupForm from "@/component/Signup/SignUpForm";
import React from "react";

// Functional component representing the index page
const Index = () => {
  return (
    <div>
      {/* Rendering the SignupForm component */}
      <SignupForm />
    </div>
  );
};

export default Index;
