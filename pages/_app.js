import { useRouter } from "next/router";
import "@/styles/globals.css";
import NewNavbar from "@/component/Navbar/NewNavbar";

// Main function to render the entire application
export default function App({ Component, pageProps }) {
  const router = useRouter();

  // Determine whether to render the navbar based on the current route
  const shouldRenderNavbar = ![
    "/sign-up",
    "/",
    "/stream",
    "/records",
    "/viewpatient",
  ].includes(router.pathname);

  return (
    // Main container for the application
    <main className="">
      {/* Render the navbar if shouldRenderNavbar is true */}
      {shouldRenderNavbar && <NewNavbar />}
      {/* Render the component passed from the page */}
      <Component {...pageProps} />
    </main>
  );
}
