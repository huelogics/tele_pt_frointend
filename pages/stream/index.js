import React, { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router";
import { io } from "socket.io-client";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
//import Image from "next/image";
import Modal from "@/component/stream/Modal";

import { CSSTransition, TransitionGroup } from "react-transition-group";

const VideoCall = () => {
  const [measurementData, setMeasurementData] = useState([]);
  const [isMicMuted, setIsMicMuted] = useState(true);
  const [isCameraOff, setIsCameraOff] = useState(false);
  const [socket, setSocket] = useState(null);
  const resultCanvasRef = useRef(null);
  const canvasRef = useRef(null);
  const videoRef = useRef(null);
  const router = useRouter();
  const socketRef = useRef(null);
  const [capturedFrames, setCapturedFrames] = useState("");
  const [isMeasurementStarted, setIsMeasurementStarted] = useState("");
  const [processedimage, setProcessedimage] = useState(null);
  const [joint, setJoint] = useState();
  const [frames, setFrames] = useState();
  const [isRecording, setIsRecording] = useState(false);
  const [videoBlob, setVideoBlob] = useState(null);
  const [recorder, setRecorder] = useState(null);
  const [processedImages, setProcessedImages] = useState([]);
  const [photo, setPhoto] = useState();
  const [notes, setNotes] = useState("");
  const [procedure, setProcedure] = useState("");
  const [type, setType] = useState("");
  const [isCameraOn, setIsCameraOn] = useState(false);
  const [receivedResult, setReceivedResult] = useState(null);
  const [resultImages, setResultImages] = useState([]);
  const [responseData, setResponseData] = useState(null);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [capturedScreenshots, setCapturedScreenshots] = useState([]);
  const [showIcons, setShowIcons] = useState(false);
  const [showMoreOptions, setShowMoreOptions] = useState(false);
  const [showComponent, setShowComponent] = useState("Procedure");
  const popUpRef = useRef(null);
  const [showModal, setShowModal] = useState(false);
  const frameContainerRef = useRef(null);
  const slider = useRef(null);
  const [procedureInput, setProcedureInput] = useState("notes");
  const [selectedOption, setSelectedOption] = useState(null);

  //socket connection
  useEffect(() => {
    const newSocket = io("https://angles.huelogics.com");
    setSocket(newSocket);

    newSocket.on("connect", () => {
      console.log("Connected to server");
    });

    newSocket.on("disconnect", () => {
      console.log("Disconnected from server");
    });

    newSocket.on("processed_image", (processed_image) => {
      if (processed_image && typeof processed_image.image_data === "string") {
        const imageData = processed_image.image_data;
        setProcessedimage(imageData);
        setProcessedImages((prevImages) => [
          ...prevImages,
          processed_image.image_data,
        ]);
      } else {
        console.error("Received processed image is invalid:", processed_image);
      }
    });

    newSocket.on("error", (error) => {
      console.error("Socket error:", error);
    });

    return () => {
      newSocket.disconnect();
    };
  }, []);

  useEffect(() => {
    if (socket) {
      socket.on("frame", (frame) => {
        console.log("Send frames to server:", frame);
        setReceivedResult(frame);
      });
    }

    return () => {
      if (socket) {
        socket.off("frame");
      }
    };
  }, [socket]);

  useEffect(() => {
    const startCamera = async () => {
      try {
        const stream = await navigator.mediaDevices.getUserMedia({
          video: true,
        });
        if (videoRef.current) {
          videoRef.current.srcObject = stream;
        }
        const video = videoRef.current;
        const canvas = canvasRef.current;
        const ctx = canvas.getContext("2d");
        video.addEventListener("play", () => {
          const timerCallback = () => {
            if (!video.paused && !video.ended) {
              ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
              if (processedimage) {
                const img = new Image();

                img.src = `data:image/png;base64,${processedimage}`;
                img.crossOrigin = "anonymous";

                img.onload = () => {
                  ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
                  if (resultCanvasRef.current) {
                    const resultCtx = resultCanvasRef.current.getContext("2d");
                    resultCtx.clearRect(
                      0,
                      0,
                      resultCanvasRef.current.width,
                      resultCanvasRef.current.height
                    );
                    resultCtx.drawImage(
                      img,
                      0,
                      0,
                      resultCanvasRef.current.width,
                      resultCanvasRef.current.height
                    );
                  }
                };
                img.onerror = (error) => {
                  console.error("Error loading image:", error);
                };
              }
            }
            requestAnimationFrame(timerCallback);
          };
          requestAnimationFrame(timerCallback);
        });
      } catch (error) {
        console.error("Error starting camera:", error);
      }
    };

    startCamera();
  }, [processedimage]);

  const captureAndSendFrame = (joint) => {
    const video = videoRef.current;
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");

    if (video.paused || video.ended) {
      return;
    }

    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

    const frameData = canvas.toDataURL("image/jpeg").split(",")[1];

    if (socket && joint) {
      const data = {
        joint: joint,
        frame: frameData,
      };
      socket.emit("data", data);
    } else {
      console.error("Socket is not initialized or joint is missing");
    }
  };

  useEffect(() => {
    let intervalId;
    let frameCount = 0;

    if (isMeasurementStarted) {
      intervalId = setInterval(() => {
        captureAndSendFrame("Left_Elbow_Extension");
        frameCount++;
        if (frameCount > 10) {
          frameCount = 0;
          renderProcessedFrame();
        }
      }, 1000);
    } else {
      clearInterval(intervalId);
    }

    return () => clearInterval(intervalId);
  }, [isMeasurementStarted]);

  useEffect(() => {
    if (processedimage) {
      const img = new Image();
      img.src = "data:image/jpeg;base64," + processedimage;
      img.crossOrigin = "anonymous";

      img.onload = () => {
        img.style.width = "100%";
        img.style.height = "500px";
        img.style.objectFit = "cover";
        const frameContainer = frameContainerRef.current;
        if (frameContainer) {
          frameContainer.innerHTML = "";
          frameContainer.appendChild(img);
        }
      };
      img.onerror = (error) => {
        console.error("Error loading image:", error);
      };
    }
  }, [processedimage]);

  const renderProcessedFrame = () => {
    if (processedimage) {
      const imgElement = frameContainerRef.current.querySelector("img");
      imgElement.src = `data:image/jpeg;base64,${processedimage}`;
    }
  };

  const captureScreenshot = () => {
    if (frameContainerRef.current && canvasRef.current) {
      const frameContainer = frameContainerRef.current;
      const canvas = canvasRef.current;
      const ctx = canvas.getContext("2d");
      canvas.width = frameContainer.clientWidth;
      canvas.height = frameContainer.clientHeight;
      const img = new Image();
      img.crossOrigin = "anonymous";
      const imgSrc = frameContainer.querySelector("img")?.src;
      if (!imgSrc) {
        console.error("No image found in the frame container");
        return;
      }

      img.src = imgSrc;

      img.onload = () => {
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

        const screenshotData = canvas.toDataURL("image/jpeg");

        setCapturedScreenshots((prevScreenshots) => [
          ...prevScreenshots,
          screenshotData,
        ]);
      };

      img.onerror = (error) => {
        console.error("Error loading image:", error);
      };
    }
  };

  const startMeasurement = () => {
    setIsMeasurementStarted(true);
    console.log("Frame are sent  ", capturedFrames);
  };

  const stopMeasurement = () => {
    setIsMeasurementStarted(false);
    setCapturedFrames("");
  };

  const toggleMic = () => {
    setIsMicMuted((prev) => !prev);
  };

  const toggleCamera = () => {
    setIsCameraOff((prev) => !prev);

    if (isCameraOff) {
      const tracks = videoRef.current.srcObject.getTracks();
      tracks.forEach((track) => track.stop());
    }
  };

  const handleScreenshot = () => {
    if (canvasRef.current) {
      const screenshot = canvasRef.current.toDataURL("image/png");
      setCapturedScreenshots((prevScreenshots) => [
        ...prevScreenshots,
        screenshot,
      ]);
    }
  };
  const handleProcedureChange = (event) => {
    setProcedure(event.target.value);
  };

  const handleOptionChange = (event) => {
    setType(event.target.value);
  };

  const handleClickOutside = (event) => {
    if (popUpRef.current && !popUpRef.current.contains(event.target)) {
      setShowIcons(false);
    }
  };

  useEffect(() => {
    if (showIcons) {
      document.addEventListener("mousedown", handleClickOutside);
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [showIcons]);

  const endCall = () => {
    console.log("End call");
  };

  const toggleMoreOptions = () => {
    setShowMoreOptions(!showMoreOptions);
  };

  const handleOptionClick = (option) => {
    setSelectedOption(option);
  };

  const handleDelete = () => {
    if (capturedScreenshots.length > 0) {
      const updatedScreenshots = [...capturedScreenshots];
      updatedScreenshots.pop();
      setCapturedScreenshots(updatedScreenshots);
    }
  };

  const NextArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, background: "black" }}
        onClick={onClick}
      />
    );
  };

  const PrevArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, background: "black" }}
        onClick={onClick}
      />
    );
  };

  const saveData = async () => {
    try {
      const response = await fetch(
        "https://tele-backend.huelogics.com/save/records",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            id: "664f1841f2a213b6bc48a10a",
            notes: notes,
            measurementData: measurementData,
          }),
        }
      );

      if (response.ok) {
        const data = await response.json();
        console.log("Data saved successfully:", data);
        // Optionally update state or UI based on the response
      } else {
        console.error("Failed to save data");
      }
    } catch (error) {
      console.error("Error saving data:", error);
    }
  };

  const jointOption = [
    "Ankle_Eversion",
    "Ankle_Eversion",
    "Ankle_Inversion",
    "Ankle_Inversion",
    "Ankle_Dorsiflexion",
    "Ankle_Dorsiflexion",
    "Ankle_Plantarflexion",
    "Ankle_Plantarflexion",
    "Elbow_Extension",
    "Elbow_Extension",
    "Elbow_Flexion",
    "Elbow_Flexion",
    "Shoulder_Abduction",
    "Shoulder_Abduction",
    "Shoulder_Flexion",
    "Shoulder_Flexion",
    "Shoulder_Horizontal_Abduction",
    "Shoulder_Horizontal_Abduction",
    "Shoulder_Internal_Rotation",
    "Shoulder_Internal_Rotation",
    "Shoulder_External_Rotation",
    "Shoulder_External_Rotation",
    "Ankle_Elevation",
    "Ankle_Elevation",
    "Ankle_Depression",
    "Ankle_Depression",
    "Wrist_Radial_Deviation",
    "Wrist_Ulnar_Deviation",
    "Wrist_Extension",
    "Wrist_Flexion",
    "Hip_Abduction",
    "Hip_Adduction",
    "Hip_Extension",
    "Hip_Flexion",
    "Hip_External_Rotation",
    "Hip_Internal_Rotation",
    "Knee_Extension",
    "Knee_Flexion",
    "Forearm_Supination",
    "Forearm_Pronation",
    "Wrist_Supination",
    "Wrist_Pronation",
  ];

  var settings = {
    dots: false,
    arrows: true,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <div className=" mx-auto flex font-sans">
      <div className="w-full text-black ">
        {/* Navbar */}
        <div className="relative p-3 flex items-center right-0 md:justify-start border-b border-gray-200 top-0 bg-white">
          <div className="flex items-center ml-auto bg-gray-100 px-4 py-4 w-fit rounded-full gap-10">
            <div className="flex gap-2 items-center">
              <img src="/Rectangle.png" alt="Image" className="h-10 w-10" />
              <p className="text-sm">Therapist</p>
            </div>

            <button
              onClick={() => setShowIcons(!showIcons)}
              className="h-6 w-6 mx-2 focus:outline-none"
            >
              &#8230;
            </button>
          </div>
          {showIcons && (
            <div
              ref={popUpRef}
              className=" top-6 bg-white border border-gray-300 shadow-lg p-2 gap-3 flex-col flex"
            >
              <div className="flex items-center ">
                <img src={`/bell.png`} width={32} height={32} loading="lazy" />
                <div className="absolute">
                  <div className="px-1 py-px text-xs rounded-full bg-red-600">
                    1
                  </div>
                </div>
                <span className="ml-2">Notification</span>
              </div>

              <div className="flex items-center relative">
                <img
                  src={`/envelope1.png`}
                  width={32}
                  height={32}
                  loading="lazy"
                />
                <div className="absolute">
                  <div className="px-1 py-px text-xs rounded-full bg-red-600">
                    1
                  </div>
                </div>
                <span className="ml-2">Message</span>
              </div>
            </div>
          )}
        </div>

        {/* Video Part */}
        <div className="grid grid-cols-8 bg-white">
          <div
            className={`bg-[#EAE9E9] px-5 py-2 ${
              showComponent !== "" ? "col-span-6" : "col-span-8"
            }`}
          >
            <div className="flex flex-col gap-5 bg-white">
              <div>
                {isMeasurementStarted && (
                  <div
                    id="frame-container "
                    className="w-full h-full object-cover videoElement"
                    ref={frameContainerRef}
                  >
                    <div>
                      <img
                        id="frame-container "
                        className="w-full h-[500px] object-cover videoElement"
                      />
                    </div>
                  </div>
                )}
                <video
                  ref={videoRef}
                  id="local-video"
                  autoPlay
                  crossOrigin="anonymous"
                  playsInline
                  muted={isMicMuted}
                  className={`w-full object-cover videoElement ${
                    isMeasurementStarted
                      ? "invisible h-0 appearance-none"
                      : "visible h-[500px]"
                  }`}
                ></video>

                <canvas
                  crossOrigin="anonymous"
                  ref={canvasRef}
                  className="w-full h-full object-cover"
                  style={{ display: "none" }}
                ></canvas>
              </div>

              <div className="flex flex-col">
                {capturedScreenshots.length > 0 ? (
                  <>
                    <img
                      src="/Trash.png"
                      alt="Trash"
                      className="flex h-6 w-6 mx-2 cursor-pointer"
                      onClick={handleDelete}
                    />
                    <Slider ref={slider} {...settings} className="screenshots">
                      {capturedScreenshots.map((screenshot, index) => (
                        <div
                          key={index}
                          className="w-full px-5 border-b border-gray-300 object-cover"
                        >
                          <CSSTransition
                            in={true}
                            appear={true}
                            exit={true}
                            timeout={300}
                            classNames="screenshot"
                          >
                            <img
                              src={screenshot}
                              alt={`Screenshot ${index + 1}`}
                              width={1000}
                              height={100}
                              className="w-60"
                            />
                          </CSSTransition>
                        </div>
                      ))}
                    </Slider>
                  </>
                ) : (
                  <div className="flex justify-center items-center h-full text-gray-500">
                    None Captured
                  </div>
                )}
              </div>
              <div className=" w-full bg-white sticky bottom-0 py-5">
                <div className="flex gap-5 items-center justify-end">
                  <button className="bg-blue-600 p-2 rounded-full ">
                    <svg
                      width="28"
                      height="28"
                      viewBox="0 0 28 28"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M18.6663 8.55566H9.7388H9.33301V14.7103L12.9852 18.6668H15.42L18.6663 15.1499V8.55566Z"
                        fill="white"
                        fill-opacity="0.8"
                      />
                      <path
                        d="M13.9997 18.083C16.578 18.083 18.6663 15.9947 18.6663 13.4163V6.99967C18.6663 4.42134 16.578 2.33301 13.9997 2.33301C11.4213 2.33301 9.33301 4.42134 9.33301 6.99967V13.4163C9.33301 15.9947 11.4213 18.083 13.9997 18.083Z"
                        stroke="white"
                        stroke-width="2.25"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M5.0752 11.2588V13.2421C5.0752 18.1655 9.07686 22.1671 14.0002 22.1671C18.9235 22.1671 22.9252 18.1655 22.9252 13.2421V11.2588"
                        stroke="white"
                        stroke-width="2.25"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M14 22.167V25.667"
                        stroke="white"
                        stroke-width="2.25"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                  </button>
                  <button className="bg-blue-600 p-2 rounded-full">
                    <svg
                      width="28"
                      height="28"
                      viewBox="0 0 28 28"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M14.619 23.8234H7.24565C3.55898 23.8234 2.33398 21.3734 2.33398 18.9118V9.08842C2.33398 5.40176 3.55898 4.17676 7.24565 4.17676H14.619C18.3057 4.17676 19.5307 5.40176 19.5307 9.08842V18.9118C19.5307 22.5984 18.294 23.8234 14.619 23.8234Z"
                        stroke="white"
                        stroke-width="2.25"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M22.7726 19.9502L19.5293 17.6752V10.3135L22.7726 8.03849C24.3593 6.93015 25.666 7.60682 25.666 9.55515V18.4452C25.666 20.3935 24.3593 21.0702 22.7726 19.9502Z"
                        stroke="white"
                        stroke-width="2.25"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M13.416 12.834C14.3825 12.834 15.166 12.0505 15.166 11.084C15.166 10.1175 14.3825 9.33398 13.416 9.33398C12.4495 9.33398 11.666 10.1175 11.666 11.084C11.666 12.0505 12.4495 12.834 13.416 12.834Z"
                        stroke="white"
                        stroke-width="2.25"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                  </button>
                  <button
                    onClick={() => setShowComponent("Chat")}
                    className="bg-blue-100 p-2 rounded-full"
                  >
                    <svg
                      width="28"
                      height="28"
                      viewBox="0 0 28 28"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M9.91732 22.1673H9.33398C4.66732 22.1673 2.33398 21.0007 2.33398 15.1673V9.33398C2.33398 4.66732 4.66732 2.33398 9.33398 2.33398H18.6673C23.334 2.33398 25.6673 4.66732 25.6673 9.33398V15.1673C25.6673 19.834 23.334 22.1673 18.6673 22.1673H18.084C17.7223 22.1673 17.3723 22.3423 17.1507 22.634L15.4007 24.9673C14.6307 25.994 13.3707 25.994 12.6007 24.9673L10.8507 22.634C10.664 22.3773 10.2323 22.1673 9.91732 22.1673Z"
                        stroke="#0060FF"
                        stroke-width="2.25"
                        stroke-miterlimit="10"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M18.6619 12.8343H18.6724"
                        stroke="#0060FF"
                        stroke-width="3"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M13.9949 12.8343H14.0054"
                        stroke="#0060FF"
                        stroke-width="3"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M9.32693 12.8343H9.3374"
                        stroke="#0060FF"
                        stroke-width="3"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                  </button>
                  <button
                    onClick={() => setShowModal(true)}
                    className="bg-blue-100 p-2 rounded-full"
                  >
                    <svg
                      width="28"
                      height="28"
                      viewBox="0 0 28 28"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M14.0003 11.666C12.717 11.666 11.667 12.716 11.667 13.9993C11.667 15.2827 12.717 16.3327 14.0003 16.3327C15.2837 16.3327 16.3337 15.2827 16.3337 13.9993C16.3337 12.716 15.2837 11.666 14.0003 11.666ZM21.0003 11.666C19.717 11.666 18.667 12.716 18.667 13.9993C18.667 15.2827 19.717 16.3327 21.0003 16.3327C22.2837 16.3327 23.3337 15.2827 23.3337 13.9993C23.3337 12.716 22.2837 11.666 21.0003 11.666ZM7.00033 11.666C5.71699 11.666 4.66699 12.716 4.66699 13.9993C4.66699 15.2827 5.71699 16.3327 7.00033 16.3327C8.28366 16.3327 9.33366 15.2827 9.33366 13.9993C9.33366 12.716 8.28366 11.666 7.00033 11.666Z"
                        fill="#0060FF"
                      />
                    </svg>
                  </button>
                  <button
                    onClick={captureScreenshot}
                    className={`py-3 px-7 rounded-full text-white font-semibold ml-72 ${
                      isMeasurementStarted
                        ? "bg-blue-700"
                        : "bg-gray-400 cursor-not-allowed"
                    }`}
                    disabled={!isMeasurementStarted}
                  >
                    Capture
                  </button>
                  <button className=" py-3 px-7 rounded-full bg-red-600 text-white font-semibold">
                    End call
                  </button>
                </div>
              </div>
            </div>
          </div>

          {showComponent === "Procedure" && (
            <div
              className={`${showComponent === "Procedure" ? "col-span-2" : ""}`}
            >
              <div className="bg-gray-100 h-full p-5">
                <div className="flex flex-col gap-5">
                  <select
                    className="py-3 font-sans rounded-full outline-none px-5"
                    name="Procedure"
                    id="Procedure"
                  >
                    <option className="text-black" value="">
                      Select Procedure
                    </option>
                    {jointOption.map((joint, index) => (
                      <option className="text-black" value={joint} key={index}>
                        {joint.replace(/_/g, " ")}
                      </option>
                    ))}
                  </select>
                  <select
                    className="py-3 font-sans rounded-full outline-none px-5"
                    name="Procedure"
                    id="Procedure"
                  >
                    <option className="text-black" value="">
                      Select Option
                    </option>
                    <option className="text-black" value="Left Arm">
                      Left
                    </option>
                    <option className="text-black" value="Left Arm">
                      Right
                    </option>
                  </select>
                  {!isMeasurementStarted ? (
                    <button
                      onClick={startMeasurement}
                      className="px-5 bg-white text-black py-3 rounded-full focus:outline-none "
                    >
                      Start Measurement
                    </button>
                  ) : (
                    <button
                      onClick={stopMeasurement}
                      className="bg-white text-black px-4 py-3 rounded-full focus:outline-none mr-4"
                    >
                      Stop Measurement
                    </button>
                  )}

                  <div className="flex justify-center items-center">
                    <div className="grid grid-cols-2 bg-blue-200 rounded-full">
                      <button
                        onClick={() => setProcedureInput("notes")}
                        className={` ${
                          procedureInput === "notes" ? "bg-blue-500" : ""
                        } rounded-l-full py-2 px-5 h-full`}
                      >
                        <p
                          className={`font-sans font-semibold text-base ${
                            procedureInput === "notes"
                              ? "text-white"
                              : "text-blue-500"
                          }`}
                        >
                          Notes
                        </p>
                      </button>
                      <button
                        onClick={() => setProcedureInput("records")}
                        className={` px-5 py-2 rounded-r-full  ${
                          procedureInput === "records" ? "bg-blue-500" : ""
                        }`}
                      >
                        <p
                          className={`font-sans font-semibold text-base ${
                            procedureInput === "records"
                              ? "text-white"
                              : "text-blue-500"
                          }`}
                        >
                          Records
                        </p>
                      </button>
                    </div>
                  </div>

                  {procedureInput === "notes" ? (
                    <div className="flex flex-col gap-5">
                      <textarea
                        className="outline-none rounded-lg p-4"
                        rows="13"
                        cols="2"
                        name="notes"
                        id="notes"
                      />
                      <div className="flex justify-center items-center">
                        <button
                          onClick={saveData}
                          className="px-10 py-2 bg-blue-500 rounded-full text-white font-semibold"
                        >
                          Save
                        </button>
                      </div>
                    </div>
                  ) : (
                    <div className="flex flex-col gap-5">
                      <div className="flex flex-col gap-1">
                        <label
                          className="font-sans font-medium"
                          htmlFor="procedure"
                        >
                          Procedure
                        </label>
                        <input
                          type="text"
                          name="procedure"
                          id="procedure"
                          className="py-2 rounded-md bg-white px-4 shadow border outline-none"
                        />
                      </div>
                      <div className="flex flex-col gap-1">
                        <label className="font-sans font-medium" htmlFor="type">
                          Type
                        </label>
                        <input
                          type="text"
                          name="type"
                          id="type"
                          className="py-2 rounded-md bg-white px-4 shadow border outline-none"
                        />
                      </div>

                      <div className="flex flex-col gap-1">
                        <label
                          className="font-sans font-medium"
                          htmlFor="devicMeasurement"
                        >
                          Measurement
                        </label>
                        <div className="grid grid-cols-2 gap-10">
                          <input
                            type="text"
                            name="devicMeasurement"
                            id="devicMeasurement"
                            className="py-2 rounded-md bg-white px-4 shadow border outline-none"
                          />
                          <input
                            type="text"
                            name="devicMeasurement"
                            id="devicMeasurement"
                            className="py-2 rounded-md bg-white px-4 shadow border outline-none"
                          />
                        </div>
                      </div>
                      <div className="flex justify-center items-center">
                        <button className="px-10 py-2 bg-blue-500 rounded-full text-white font-semibold">
                          Add
                        </button>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          )}
          {showComponent === "Chat" && (
            <div className={`${showComponent === "Chat" ? "col-span-2" : ""}`}>
              <div className="flex flex-col h-full">
                <div className="bg-white border-b border-gray-200 p-5 flex items-center ">
                  <img
                    onClick={() => setShowComponent("Procedure")}
                    src="/Back.png"
                    alt="Image"
                    className="h-5 w-5  cursor-pointer"
                  />

                  <p className="ml-5 font-semibold text-lg text-black">Chats</p>
                </div>
                <div className="bg-gray-50 h-full"></div>
                <div className="w-full bg-white sticky bottom-0 px-3 py-5">
                  <div className="flex items-center rounded-full bg-gray-50 outline-none border placeholder:text-gray-400 placeholder:text-sm w-full">
                    <div className="px-2">
                      <svg
                        width="16"
                        height="20"
                        viewBox="0 0 20 27"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M9.96289 13V17.375C9.96289 19.7875 11.9254 21.75 14.3379 21.75C16.7504 21.75 18.7129 19.7875 18.7129 17.375V10.5C18.7129 5.6625 14.8004 1.75 9.96289 1.75C5.12539 1.75 1.21289 5.6625 1.21289 10.5V18C1.21289 22.1375 4.57539 25.5 8.71289 25.5"
                          stroke="#8D8F98"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                      </svg>
                    </div>
                    <input
                      type="text"
                      name=""
                      id=""
                      className="w-full py-2 outline-none bg-gray-50 placeholder:text-sm font-sans mx-2"
                      placeholder="Type something..."
                    />
                    <div>
                      <svg
                        width="50"
                        height="50"
                        viewBox="0 0 50 50"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g filter="url(#filter0_b_1003_503)">
                          <rect width="50" height="50" rx="25" fill="#0060FF" />
                        </g>
                        <path
                          d="M19.2496 17.9002L29.8621 14.3627C34.6246 12.7752 37.2121 15.3752 35.6371 20.1377L32.0996 30.7502C29.7246 37.8877 25.8246 37.8877 23.4496 30.7502L22.3996 27.6002L19.2496 26.5502C12.1121 24.1752 12.1121 20.2877 19.2496 17.9002Z"
                          stroke="white"
                          stroke-width="2.2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M22.6377 27.0627L27.1127 22.5752"
                          stroke="white"
                          stroke-width="2.2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <defs>
                          <filter
                            id="filter0_b_1003_503"
                            x="-7"
                            y="-7"
                            width="64"
                            height="64"
                            filterUnits="userSpaceOnUse"
                            color-interpolation-filters="sRGB"
                          >
                            <feFlood
                              flood-opacity="0"
                              result="BackgroundImageFix"
                            />
                            <feGaussianBlur
                              in="BackgroundImageFix"
                              stdDeviation="3.5"
                            />
                            <feComposite
                              in2="SourceAlpha"
                              operator="in"
                              result="effect1_backgroundBlur_1003_503"
                            />
                            <feBlend
                              mode="normal"
                              in="SourceGraphic"
                              in2="effect1_backgroundBlur_1003_503"
                              result="shape"
                            />
                          </filter>
                        </defs>
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
      {showModal && <Modal setShowModal={setShowModal} />}
    </div>
  );
};

export default VideoCall;
