import LoginForm from "@/component/Login/LoginForm";
import Head from "next/head";

export default function Home() {
  return (
    <div className="">
      <Head>
        {" "}
        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap"
          rel="stylesheet"
        />
      </Head>
      <LoginForm />
    </div>
  );
}
